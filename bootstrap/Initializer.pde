import dawesometoolkit.*;
DawesomeToolkit dawesome;

Palette palette;

void init () {
  palette = new Palette(new color[] { #424b54, #e1ce7a, #dde0bd, #470024, #0a100d }, PaletteLerpMode.LINEAR); // generated with https://coolors.co/app
  palette.setBackground(#2f2f3f);
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}