add_library('sound')

import Initializer

SCALE = 80.0
W = int(SCALE * 16)
H = int(SCALE * 9)

PADDING = W // 20

orchestrator = None
media = None
song = None

DEBUG = False

def settings():
    size(W, H)
    smooth()

def setup():
    Initializer.init()

    blend_mode(DIFFERENCE)
    
    media = Media(this, "mp3")
    song = media.load_audio(media.get_file(2))
    song.jump(50) # seconds
    song.pause()
    orchestrator = BlobbyOrchestrator(this, media)

    background(0)

    orchestrator.setup()
    orchestrator.play()

def draw():
    orchestrator.orchestrate()
    
    texturize()

def texturize():
    no_fill()
    lines = 200
    for i in range(lines):
        # Scratches
        stroke(255)
        mag = 2 * (noise(i) * 9 + 1)
        stroke_weight(1 / mag)
        direction = PVector.random2D().set_mag(mag)
        origin = PVector(random(width), random(height))
        line(origin.x, origin.y, origin.x + direction.x, origin.y + direction.y) 

        # Scanlines
        if i % 10 == 0:
            stroke_weight(50)
            stroke(noise(i, frame_count % 20) * 255, 1)
            y = i / lines * height
            line(0, y, width, y)
        
    for i in range(200):
        stroke_weight(random(3, 5))
        if random(60) > 55:
            stroke_weight(20)
        point(random(width), random(height))
