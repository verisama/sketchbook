class Blob:
  def __init__(self, radius):
    self.nPoints = 200
    self.angle = TWO_PI / self.nPoints
    self.radius = radius
    self.min_radius = radius * 0.2
    self.noise_factor = 0.002
    self.time_factor = 0.01
    
    self.vertices = [None] * self.nPoints
    self.mutatedVertices = [None] * self.nPoints
    reset()
  
  def reset ():
    for i in range(nPoints):
      vertices[i] = calculateVertex(i)
      mutatedVertices[i] = vertices[i]
  
  def calculateVertex(int i):
    return PVector(
        cos(self.angle * i) * self.radius,
        sin(self.angle * i) * self.radius
       ).normalize()
  
  def mutate(t, noisiness, lerp=1):
    n = None

    for i in range (self.mutatedVertices.length):
      v = self.mutatedVertices[i]
      x = v.x * self.noiseFactor * noisiness + t * self.timeFactor
      y = v.y * self.noiseFactor * noisiness + t * self.timeFactor
      n = noise(x, y)
      
      targetMag = map(n, 0, 1, self.minRadius, self.radius)
      v.setMag(lerp(v.mag(), targetMag, lerp))
  
  def getColor(v):
    return palette.getColor(v.mag())

  def paint(x, y, noisiness, _mode, magFactor):
    pushMatrix()
    translate(x, y)

    beginShape()
    
    for v in mutatedVertices:
      w = v.copy().setMag(v.mag() * magFactor * (1 + noisiness * noiseFactor))
      curveVertex(w.x, w.y)

    endShape(CLOSE)
    popMatrix()

    def paintStyled(x, y, _noisiness, mode, magFactor=1):
      pushMatrix()
      pushStyle()
      translate(x, y)

      beginShape()
      strokeCap(PROJECT)

      for v in self.mutatedVertices:
        if (mode == "fill"):
          fill(getColor(v))
        elif (mode == "stroke"):
          strokeWeight(radius / 5)
          stroke(getColor(v))
        
        w = v.copy().setMag(v.mag() * magFactor)
        curveVertex(w.x, w.y)

      endShape(CLOSE)
      popStyle()
      popMatrix()
  
  
