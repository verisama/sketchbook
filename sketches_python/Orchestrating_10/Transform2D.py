import math

class Transform2D:
    def __init__(self, position=None, scale=None, rotation=0):
        if position is None:
            self.position = [0, 0]
        else:
            self.position = position.copy()
        if scale is None:
            self.scale = [1, 1]
        else:
            self.scale = scale.copy()
        self.rotation = rotation
        self.x, self.y = self.position

    def move(self, direction, speed=1):
        self.position[0] += direction[0] * speed
        self.position[1] += direction[1] * speed
        self.x, self.y = self.position

    def turn_degrees(self, rotation):
        self.rotation += math.radians(rotation)

    def turn_radians(self, rotation):
        self.rotation += rotation

    def begin_matrix(self):
        import numpy as np
        self.transform_matrix = np.array([[np.cos(self.rotation), -np.sin(self.rotation), self.position[0]],
                                         [np.sin(self.rotation), np.cos(self.rotation), self.position[1]],
                                         [0, 0, 1]])

    def end_matrix(self):
        pass

    def in_bounds(self, x0, y0, x1, y1):
        min_x = min(x0, x1)
        max_x = max(x0, x1)
        min_y = min(y0, y1)
        max_y = max(y0, y1)
        return self.position[0] > min_x and self.position[0] < max_x and self.position[1] > min_y and self.position[1] < max_y
