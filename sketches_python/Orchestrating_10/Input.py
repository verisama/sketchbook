import random

version_code = str(random.randint(1, 1000)).hashcode()

def mouse_pressed():
    print(song.position())
    if song.is_playing():
        song.pause()
    else:
        song.play()

def key_pressed():
    if key in ['s', 'S']:
        name = Path(__file__).stem
        filename = f"{name}_{version_code}_####.png"
        save_frame(filename)
    elif key in ['f', 'F']:
        song.jump(song.position() + 1)
