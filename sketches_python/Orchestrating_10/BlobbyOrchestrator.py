import orchestrator

class BlobbyOrchestrator(orchestrator.Orchestrator):
  
  def __init__ (self,  context, media):
    super(context, media)
    self.intensityThreshold = 10
    self.bandScale = 100

    self.dynamicLevel = true
    
    self.blob = Blob(height / 2)
    
    dancers = []
    
    center = center()

  def setup ():

    colorMode(HSB, 360, 100, 100, 100)
  
  def center ():
    return PVector(width / 2, height / 2)
  def up ():
   return PVector(0, -1)
  def down ():
   return PVector(0, 1)
  def left ():
   return PVector(-1, 0)
  def right ():
   return PVector(1, 0)
  
  def always():
    super.always()

    # background(0)
    palette.drawBackground(128)

    blob.mutate(frameCount, sinceLastPeak())

  def onBeat ():
    super.onBeat()
    background(128)

  def onLevelPeak ():
    super.onLevelPeak()

  def onLevelMid ():
    super.onLevelMid()
    fill(180, 80, 80)

  def onLevelLow ():

    super.onLevelLow()
    fill(90, 80, 80)
  
  def addDancer(direction, size, speed, lifetime):
    dancer =  BlobbyDancer(center(), direction, size, speed, lifetime)
    self.dancers.add(dancer)
  
  def addDancerUp( size,  speed,  lifetime):
    addDancer(up(), size, speed, lifetime)
  
  def addDancerDown( size,  speed,  lifetime):
    addDancer(down(), size, speed, lifetime)

  def addDancerLeft( size,  speed,  lifetime):
    addDancer(left(), size, speed, lifetime)

  def addDancerRight(size, speed, lifetime):
    addDancer(right(), size, speed, lifetime)

  def onBand ( band, frequency, power):
    intensity = power * bandScale; # * pow(log(band) * 3, 2)

    if (frequency > L_L_THRESHOLD and frequency <= L_U_THRESHOLD):
      onLowFrequencies(band, frequency, intensity)
    elif (frequency > M_L_THRESHOLD and frequency <= M_U_THRESHOLD):
      onMidFrequencies(band, frequency, intensity)
    elif (frequency > H_L_THRESHOLD and frequency <= H_U_THRESHOLD):
      onHighFrequencies(band, frequency, intensity)
    
    
    if (band < media.frequencyToBand(200)):
      if (intensity > 30):
        onKick(intensity)
      else:
        onKickOff(intensity)
    
    elif (band < media.frequencyToBand(2000)):
      if (intensity > 30):
        onMid(intensity)
    
    if (band < media.frequencyToBand(8000)):
      if (intensity > 10):
        onHigh(intensity)
    
    if (DEBUG):
      hue = map(band, 0, media.FFT_BANDS, 0, 360)
      fill(hue, 80, 80)
      circle(map(band, 0, media.FFT_BANDS, 0, width), height/2, intensity)
  
  def bandBetween( value,  lower,  upper):
    return value >= lower and value <= upper
  
  def onKick( intensity):
    fill(180, 100, 100)
    blob.paint(center(), intensity * 50, "fill", intensity / 200)
  
  def onKickOff(_intensity):
    blob.reset()
  
  def onMid(_intensity):
    background(0, 100, 100)
  
  def onHigh(intensity):
    fill(60, 100, 100)
    
    if (intensity > 30):
      side = height / sqrt(3.0)
      triangle(center.x - side, height, center.x, 0, center.x + side, height)
      return
    
    lastPeak = sinceLastPeak()
    x = map(intensity, 0, 100, width / 4, width / PHI) + lastPeak * 2
    
    y = center.y + lastPeak * randomGaussian() * 10
    circle(x, y, intensity / 100 * height * 2)
    circle(width - x, y, intensity / 100 * height * 2)

  def onLowFrequencies(band, frequency, intensity):
    pass
  def onMidFrequencies(band, frequency, intensity):
    pass
  def onHighFrequencies(band, frequency, intensity):
    pass
