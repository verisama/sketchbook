import dancer.Dancer

class BlobbyDancer(dancer.Dancer):
  def __init__(self, origin, direction, size, speed, lifespan):
    super().__init__(origin, direction, size, speed, lifespan)
    
    self.alpha = 10
    self.noiseDetail = 0.2
    self.directionNoiseFactor = 0.1

    self.size = size
    
    self.blobby = Blob(size)
    self.transform.rotation = radians(random(360))
    self.vertices = []
    
    if not inBounds():
      transform.position = PVector(width >> 1 , height >> 1)
  
  
  def inBounds():
    return transform.inBounds(
      0, 0,
      width, height
     )
  
  def update():
    super.update()
    updateDirection()
    updatePosition()
    updateRotation()
    updateScale()
  
  def updateDirection():
    n = noiseDetail * noisiness
    r = directionNoiseFactor * noisiness
    
    direction.x += noise(transform.position.y * n) * randomGaussian() * r
    direction.y += noise(transform.position.x * n) * randomGaussian() * r
  
  def updatePosition():
    transform.move(direction, speed)
    
    if not inBounds():
      circle(transform.position.x, transform.position.y, size * 1.2)
      invertDirection()
      transform.move(direction, speed * 2)
  
  def updateRotation():
    transform.turnDegrees(3 * noise(transform.position.x, transform.position.y, noiseDetail * noisiness))
  
  def updateScale():
    blobby.mutate(frameCount, noisiness)
  
  def setSpeed(v):
    self.speed = v
  
  def setAlpha(v):
    self.alpha = v
  
  def invertDirection():
    direction.x = -direction.x
    direction.y = -direction.y

  def draw1(c):
    blobby.paint(0,0, noisiness, "fill")
  
  def draw2(c):
    blobby.paint(0,0, noisiness, "stroke")
