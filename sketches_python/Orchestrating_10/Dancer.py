#
# https://www.codeconvert.ai/java-to-python-converter
#
class Dancer:
  def __init__(self, origin, direction, size, speed, lifespan=0):
    self.transform = None
    self.direction = None
      
    self.dead = False
    self.mortal = False
      
    self.noisiness = 1
    self.transform = Transform2D(origin, PVector(size, size, size))
    self.direction = direction
    
    self.speed = speed
    self.rotationSpeed = speed
    
    self.lifetime = 0
    self.lifespan = lifespan

    if lifespan:
      self.mortal = True
  
  def setNoisiness(noisiness):
    self.noisiness = noisiness
  
  def update():
    if (dead):
      return
    if (mortal and lifetime >= lifespan):
      self.dead = true
    
    lifetime += 1
  
  def draw(int mode, color c):
    if (dead):
      return
    
    transform.beginMatrix()
    
    match(mode):
      case 1:
        draw1(c)
      case 2:
        draw2(c)
      case 3:
        draw3(c)
    
    transform.endMatrix()
  
  def draw1(c):
    ellipse(transform.x, transform.y, transform.scale.x, transform.scale.y)
  
  def draw2(c):
    ellipse(transform.x, transform.y, transform.scale.x, transform.scale.y)
  
  def draw3(c):
    ellipse(transform.x, transform.y, transform.scale.x, transform.scale.y)
