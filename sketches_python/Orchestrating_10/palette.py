add_library('colorharmony')

import enum.Enum

PaletteLerpMode = Enum('PaletteLerpMode', 'LINEAR', 'LOG', 'EXP')
PaletteCreateMode = Enum('PaletteCreateMode', 'MONOCHROMATIC', 'ANALOGOUS', 'COMPLEMENTARY', 'TRIADS')

class Palette:
  # color[] colors
  # color foreground
  # color background
  # PaletteLerpMode mode
  
  # ColorHarmony colorHarmony
  
  def __init__(self, colors):
    hexColors = colors.split("-")
    self.mode = PaletteLerpMode.LINEAR
    self.colors = [None] * 5
    
    for i in range(len(hexColors)):
      self.colors[i] = unhex("FF" + hexColors[i])

  ##
  # Get Color Methods
  #
  
  def getColor(v=noise(frameCount, random(1)), max=len(colors), mode=self.mode):
    v = constrainValue(v, max, mode)
    
    c1 = floor(v * len(colors))
    c2 = ceil(v * len(colors))
    
    if (c1 == c2):
      c2 += 1
    
    v = map(v, c1 / len(colors), c2 / len(colors), 0, 1)
    
    if (c2 == len(colors)):
      c2 = 0
    
    return lerpColor(colors[int(c1)], colors[int(c2)], v)
  
  
  def constrainValue (v,  max, mode):
    v = abs(v)
    
    if (v > 1):
      v = (v % max) / max
    
    match(mode):
      case PaletteLerpMode.LINEAR:
        pass
      case PaletteLerpMode.EXP:
        v = map(exp(v), exp(0), exp(1), 0, 1)
      case PaletteLerpMode.LOG:
        v = map(log(v * 10), log(1), log(10), 0, 1)
    
    v = v % 1.0
    
    return max(0, v)
  
  def getColorDiscrete(v, size, mode):
    return getColor(floor(v * size) / (float)size, mode)

  ##
  # Special Color Methods
  #
  
  def setForeground(color):
    self.foreground = color
  def setBackground(color):
    self.background = color
    
  def fg(): 
    return foreground
  def bg(): 
    return background

  def lightest():
    cs = sorted(sprcolors.copy())
    return cs[len(cs) - 1]

  def darkest():
    cs = sorted(colors.copy())
    return cs[0]
  
  def drawBackground (alpha=255):
    pushStyle()
    noStroke()
    fill(background, alpha)
    rect(0, 0, width, height)
    popStyle()

  # Debug methods 
  def draw ():
    pushStyle()
    noFill()
    strokeWeight(1)
    for x in range(width):
      v = float(x / width)
      c = getColorDiscrete(v, 36)
      stroke(c)
      line(x, 0, x, height)
    popStyle()
