import math
import palette

PHI = 1 + math.sqrt(5) / 2.0

def init():
    chosen_palette = 1
    palettes = [
        "63372c-c97d60-F9DBBD-f2e5d7-DB5461",
        "a1e8af-94c595-747c92-372772-3a2449",
        "42033d-680e4b-7c238c-854798-7c72a0"
    ]

    palette = Palette(palettes[chosen_palette], PaletteLerpMode.EXP) # generated with https://coolors.co/app
    palette.set_background(0x101010)
