PHASE = 180.0
PETALS = 7

COLOR_STEP = 12.5

def setup():
    size(800,800)
    colorMode(HSB, 360, 100, 100, 100)
    rectMode(CENTER)
    

def draw():
    noFill()
    background(180 + COLOR_STEP, 100, 80)
    
    phase = altPhase = PHASE
    
    if (frameCount % phase == 0):
        phase = phase + randomGaussian() * 10
    
    step = frameCount % altPhase
    bouncyStep = abs(step - altPhase / 2)
    bouncyDelta = sqrt(pow(bouncyStep, 2.6)) / altPhase
    
    total = PETALS
    
    for i in range(int(total)):
        petal(TWO_PI/total * i, bouncyDelta)
        
    globalShapes(bouncyDelta)
    
def globalShapes(delta):
    pushMatrix()
    pushStyle()
    
    stroke(delta * 360, 0, 100)
    translate(width/2, height/2)
    
    fill(180 + COLOR_STEP * 4, 80, 80, 30)
    circle(0, 0, delta * width * 2)
    
    h = height / 2 * 0.9 - delta * height / PI
    r = PETALS / 2.0
    with beginClosedShape():
        for i in range(PETALS):
            x = cos(i * TWO_PI/r) * h
            y = sin(i * TWO_PI/r)* h
            circle(x, y, delta * width / TWO_PI)
            vertex(x, y)

    
    strokeWeight(8)
    circle(0, 0, delta * width / PETALS / HALF_PI)
    strokeWeight(2)
    fill(180, 80, 80, 40)
    circle(0, 0, delta * width / PETALS)
    circle(0, 0, delta * width / TWO_PI)
    circle(0, 0, delta * width / HALF_PI)
    
    popStyle()
    popMatrix()


def petal(rotation, delta):
    pushMatrix()
    pushStyle()
    
    translate(width/2, height/2)
    rotate(rotation)

    l1 = 12
    l2 = 6 * delta
    l3 = 36  * delta
    
    h1 = width/PI
    h2 = width/TWO_PI
    
    h3 = (h1+h2)/2
    
    noStroke()
    fill(0, 0, 100)
    
    
    
    circle(h1 * cos(delta * HALF_PI), 0, l1)
    noFill()
    stroke(0,0,100)
    circle(h1 * cos(delta * HALF_PI), 0, l1 * PI)
    
    for i in range(PETALS):
        circle(h2 + h1 * i, 0, l2)
    
    stroke(0,0,100)
    fill(180 + COLOR_STEP * 3, 100, 90, 80)
    
    
    h4 = height / ((l2 * 5) + 1)    
    h4_2 = h4 * 2 * delta
    
    circle(h4, h4, l1 * PI)
    
    with beginClosedShape():
        curveVertex(h4, h4)
        curveVertex(h4_2, h4)
        curveVertex(h4_2, h4_2)
        curveVertex(h4, h4_2)
        curveVertex(h4, h4)
    
    popStyle()
    popMatrix()
