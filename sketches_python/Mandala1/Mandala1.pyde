PHASE = 120.0

def setup():
    size(800,800);


def draw():
    total = 32.0
    noFill()
    background("#ffffff")
    
    phase = altPhase = PHASE
    
    if (frameCount % phase == 0):
        phase = phase + randomGaussian() * 10
    
    step = frameCount % altPhase
    bouncyStep = step - altPhase / 2
    bouncyDelta = abs(sqrt(pow(bouncyStep, 2))) / altPhase
    

    
    for i in range(int(total)):
        petal(TWO_PI/total * i, bouncyDelta);
    
    
    
def petal(rotation, delta):
    pushMatrix();
    translate(width/2, height/2);
    rotate(rotation);
    line(0,0, height, width/2);
    
    
    
    l1 = 3
    l2 = 6 * delta
    l3 = 36  * delta
    
    h1 = width/PI
    h2 = width/TWO_PI
    
    h3 = (h1+h2)/2
    
    circle(h1, 0, l1);
    circle(h2, 0, l2);
    
    line(-l2, h3, l2, h3);
    
    triangle(-l3, 0, 0, h3, l3, 0);
    
    popMatrix();
