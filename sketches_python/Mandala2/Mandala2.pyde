PHASE = 180.0
PETALS = 16

def setup():
    size(800,800)
    colorMode(HSB, 360, 100, 100, 100)
    rectMode(CENTER)


def draw():
    
    noFill()
    background("#cc0000")
    
    phase = altPhase = PHASE
    
    if (frameCount % phase == 0):
        phase = phase + randomGaussian() * 10
    
    step = frameCount % altPhase
    bouncyStep = step - altPhase / 2
    bouncyDelta = abs(sqrt(pow(bouncyStep, 2))) / altPhase
    
    
    
    total = PETALS
    
    if (frameCount % 120 < 60):
        total = PETALS + int(PETALS * bouncyDelta) % 12
    
    for i in range(int(total)):
        petal(TWO_PI/total * i, bouncyDelta)
        
    globalShapes(bouncyDelta)
    
def globalShapes(delta):
    pushMatrix()
    pushStyle()
    
    stroke(delta * 360, 0, 100)
    translate(width/2, height/2)
    circle(0, 0, delta * width * 2)
    rect(0, 0, width * 0.8 - delta * width/PI, width* 0.8 - delta * width/PI)
    
    strokeWeight(8)
    circle(0, 0, delta * width / PETALS / HALF_PI)
    
    popStyle()
    popMatrix()


def petal(rotation, delta):
    pushMatrix()
    pushStyle()
    
    translate(width/2, height/2)
    rotate(rotation)
    
    strokeWeight(8)
    # stroke(degrees(rotation), 80, 80, 40)
    
    # line(0,0, height, width/2)
    
    l1 = 12
    l2 = 6 * delta
    l3 = 36  * delta
    
    h1 = width/PI
    h2 = width/TWO_PI
    
    h3 = (h1+h2)/2
    
    
    
    
    
    # line(-l2, h3, l2, h3)
    
    strokeWeight(1)
    for i in range(PETALS):
        stroke(i / float(PETALS) * 60, 80, 80, 100)
        h = h3 / exp(delta) + i * 8
        l = l3 + i
        line(-l, 0, 0, h)
        line(l, 0, 0, h)
    
    
    #if (rotation % HALF_PI == 0):
    noStroke()
    fill(0, 0, 100)
    circle(h1 * cos(delta * HALF_PI), 0, l1)
    
    for i in range(PETALS):
        circle(h2 + h1 * i / 8, 0, l2)    
    
    popStyle()
    popMatrix()
