color[] palette = new color[]{ #333333, #aaaaaa };

void setup () {
  size(512, 512);
  colorMode(HSB, 360, 100, 100, 100);
  generate();
}

void draw () {}

void generate () {
  background(getColor(0));
  drawEllipse();
  
  texturizeAll();
}

void drawEllipse () {
  color c = getColor(1);
  float size = height / PHI;
  pushMatrix();
  pushStyle();
  
  translate(width / 2, height / 2);
  
  noStroke();
  fill(c);
  ellipse(0, 0, size / 2, size / 2);
  
  noFill();
  stroke(c);
  strokeWeight(2);
  ellipse(0, 0, size, size);
  
  popStyle();
  popMatrix();
}

void keyPressed () {
  if (key == 'g' || key =='G') {
    generate();
  }
  else if (key == 's' || key == 'S') {
    saveTheFrame();
  }
  else if (key == 't' || key == 'T') {
    texturize = !texturize;
  }
}
