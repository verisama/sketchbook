import java.util.Date;

float initialSize = 200;
float size = initialSize;
color bg = #651386;

float speed = 2;

PVector origin;

boolean SAVE_FRAMES = true;
int SAVE_FRAME_RATE = 6;

boolean grow = false;

float minSize;

void setup (){
  background(bg);
  blendMode(DIFFERENCE);
  size(800, 800);
  textAlign(CENTER, CENTER);
  //size(1080, 1920);
  
  minSize = width / 100;
  origin = new PVector(
    width / 2,
    height / 2
  );
  
  // for (int i = 0; i < 200; i++) paintPolygon();
}

void draw () {
  if (frameCount % 6 == 0) bg(bg, 2);
  if (grow) bg(bg, size / width);
  if (frameCount % 12 == 0) paintText();
  
  if (SAVE_FRAMES && frameCount % SAVE_FRAME_RATE == 0) saveFrame("diffidumtext_" + new Date().getTime() + "_####.tga");
}

void paintText () {
  if (grow) {
   size += pow(frameCount / 100, 3);
   if (size > minSize) size += randomGaussian() * 2;
  }
  else {
    if (size > 0) size -= random(10, 20);
  }
  
  if (size < minSize){  grow = true; }
  
  fill(255);
  textSize(size < minSize ? minSize : size);
  
  text("ADRI &", origin.x, origin.y -size);
  text("SUS", origin.x, origin.y);
  text("COSAS", origin.x, origin.y + size);
  
  origin = origin.add(randomGaussian() * speed, randomGaussian() * speed);
  

}
/*
void paintPolygon () {
  float clock = frameCount % frameRate;

  float sides = polygons[int(random(polygons.length))];

  // float radius = abs(size * randomGaussian() * (frameCount % 60) / 15);
  float radius = size * clock / random(10, 30);
  
  pushMatrix();
  pushStyle();
  translate(width / 2, height / 2);
  rotate(TWO_PI / (frameRate * 3) * clock);
  beginShape();
  noFill();
  stroke(128);
  
  float sw = pow(radius / 81, 3);
  strokeWeight(sw < 2 ? 2 : sw);

  for (int i = 0; i < sides; i++) {
    float t = TWO_PI / sides * i;
    float x = cos(t) * radius;
    float y = sin(t) * radius;
    vertex(x, y);
  }
  endShape(CLOSE);
  popStyle();
  popMatrix();
}
*/
void bg(color c, float opacity) {
  pushMatrix();
  pushStyle();
  noStroke();
  fill(c, opacity);
  rect(0, 0, width, height);
  popStyle();
  popMatrix();
}
