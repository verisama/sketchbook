import ddf.minim.*;
  Minim minim;
  AudioPlayer song;
  
  import processing.sound.*;

  
int spacing = 20;
int border = 10;
int amplification = 10;
int y = spacing;
float ySteps;  
void setup() {  
  size(200, 200);
  background(255);
  noStroke();
  minim = new Minim(this);
  song = minim.loadFile("Climb.mp3");
  song.play();
  
  colorMode(HSB, 360, 100, 100, 100);
}


FFT fft;
AudioIn in;
int bands = 512;
float[] spectrum = new float[bands];

void setup() {
  size(512, 360);
  background(255);
    
  // Create an Input stream which is routed into the Amplitude analyzer
  fft = new FFT(this, bands);
  in = new AudioIn(this, 0);
  
  // start the Audio Input
  in.start();
  
  // patch the AudioIn
  fft.input(in);
}      

void draw() { 
  background(255);
  fft.analyze(spectrum);

  for(int i = 0; i < bands; i++){
  // The result of the FFT is normalized
  // draw the line for frequency band i scaling it up by 5 to get more amplitude.
  line(i, height, i, height - spectrum[i]*height*5 );
  } 
}

void draw() {
  int screenSize = int((width-2*border)*(height-2*border)/spacing);
  int x = int(map(song.position(), 0, song.length(), 0, screenSize));
  
  ySteps = x/(width-2*border);         // calcul du nombre de lignes
  x -= (width-2*border)*ySteps;        // nouvelle posiition de x pour chaque lignes
  
  float frequency = song.mix.get(int(x))*spacing*amplification; 
  
  square(x, y*ySteps, 50);
  fill(frequency / 100 * 360, 100, 100);
}

void stop() {
  song.close();
  minim.stop();
  super.stop();
}
