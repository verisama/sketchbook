class Transform2D {
  PVector position;
  PVector scale;
  float rotation;
  
  float x, y;
  
  
  /**
  * Constructors
  */
  
  public Transform2D() {
    this.position = new PVector(0, 0);
    this.scale    = new PVector(1, 1);
    this.rotation = 0;
    updateXY();
  }
  
  public Transform2D(PVector position) {
    this.position = position.copy();
    this.scale    = new PVector(1, 1);
    this.rotation = 0;
    updateXY();
  }
  
  public Transform2D(PVector position, PVector scale) {
    this.position = position.copy();
    this.scale    = scale.copy();
    this.rotation = 0;
    updateXY();
  }
  
  public Transform2D(PVector position, PVector scale, float rotation) {
    this.position = position.copy();
    this.scale    = scale.copy();
    this.rotation = rotation;
    updateXY();
  }
  
  
  /**
  * Public methods
  */
  
  void move(PVector direction, float speed) {
    position.add(direction.copy().mult(speed));
    updateXY();
  }
  
  void move(PVector direction) {
    move(direction, 1);
  }
  
  void turnDegrees(float rotation) {
    this.rotation += radians(rotation);
  }
  
  void turnRadians(float rotation) {
    this.rotation += rotation;
  }
  
  void beginMatrix() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(rotation);   
  }
  
  void endMatrix() {
    popMatrix();
  }
  
  void updateXY() {
    x = position.x;
    y = position.y;
  }
  
  boolean inBounds(float x0, float y0, float x1, float y1) {
    float minX = min(x0, x1), maxX = max(x0, x1);
    float minY = min(y0, y1), maxY = max(y0, y1);
    
    return position.x > minX && position.x < maxX
    && position.y > minY && position.y < maxY;
  }
}
