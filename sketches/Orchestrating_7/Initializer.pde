Palette palette;
float PHI = 1 + sqrt(5) / 2.0f;

void init() {
  int chosenPalette = 1;
  String[] palettes = new String[]{
    "63372c-c97d60-F9DBBD-f2e5d7-DB5461",
    "a1e8af-94c595-747c92-372772-3a2449",
    "42033d-680e4b-7c238c-854798-7c72a0"
  };
  
  palette = new Palette(palettes[chosenPalette], PaletteLerpMode.EXP); // generated with https://coolors.co/app
  palette.setBackground(#101010);
}
