class Dancer {
  Transform2D transform;
  PVector direction;
  
  float speed, rotationSpeed;
  
  int lifetime, lifespan = 0;
  
  boolean dead = false;
  boolean mortal = false;
  
  float noisiness = 1;
  
  public Dancer(PVector origin, PVector direction, float size, float speed) {
    this.transform = new Transform2D(origin, new PVector(size, size, size));
    this.direction = direction;
    
    this.speed = speed;
    this.rotationSpeed = speed;
    
    this.lifetime = 0;
  }  
  
  public Dancer(PVector origin, PVector direction, float size, float speed, float lifespan) {
    this.transform = new Transform2D(origin, new PVector(size, size, size));
    this.direction = direction;
    
    this.speed = speed;
    this.rotationSpeed = speed;
    
    this.lifetime = 0;
    this.lifespan = (int)lifespan;
    this.mortal = true;
  }
  
  
  public void setNoisiness(float noisiness) {
    this.noisiness = noisiness;
  }
  
  
  
  void update() {
    if (dead) return;
    if (mortal && lifetime >= lifespan) dead = true;
    
    lifetime++;
  }
  
  void draw(int mode, color c) {
    if (dead) return;
    
    transform.beginMatrix();
    
    switch(mode) {
      case 1: draw1(c); break;
      case 2: draw2(c); break;
      case 3: draw3(c); break;
    }
    
    transform.endMatrix();
  }
  
  void draw1(color c) {
    ellipse(transform.x, transform.y, transform.scale.x, transform.scale.y);
  }
  
  void draw2(color c) {
    ellipse(transform.x, transform.y, transform.scale.x, transform.scale.y);
  }
  
  void draw3(color c) {
    ellipse(transform.x, transform.y, transform.scale.x, transform.scale.y);
  }
}
