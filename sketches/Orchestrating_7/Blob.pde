class Blob {
  float radius, minRadius;
  
  int nPoints = 200;
  float angle = TWO_PI / nPoints;
  float noiseFactor = 0.002f;
  float timeFactor = 0.01f;
  PVector[] vertices;
  PVector[] mutatedVertices;

  public Blob(float radius) {
    this.radius = radius;
    this.minRadius = radius * 0.2;
    this.vertices = new PVector[nPoints];
    this.mutatedVertices = new PVector[nPoints];
    reset();
  }
  
  void reset () {
    for (int i = 0; i < nPoints; i++) {
      vertices[i] = calculateVertex(i);
      mutatedVertices[i] = vertices[i];
    }
  }
  
  PVector calculateVertex(int i) {
    return new PVector(
        cos(angle * i) * radius,
        sin(angle * i) * radius
       ).normalize();
  }
  
  public void mutate(float t, float noisiness, float lerp) {
    float n;

    for (int i = 0; i < mutatedVertices.length; i++) {
    PVector v = mutatedVertices[i];
    float x = v.x * noiseFactor * noisiness + t * timeFactor;
    float y = v.y * noiseFactor * noisiness + t * timeFactor;
      n = noise(x, y);
      
      float targetMag = map(n, 0, 1, minRadius, radius); 
      v.setMag(lerp(v.mag(), targetMag, lerp));
    }
  }
  
  public void mutate(float t, float noisiness) { mutate(t, noisiness, 1); }
  
  
  public color getColor(PVector  v) {
    return palette.getColor(v.mag());
  }

  public void paint(float x, float y, float noisiness, String _mode, float magFactor) {
    pushMatrix();
    translate(x, y);

    beginShape();

    
    for (PVector v : mutatedVertices) {
      PVector w = v.copy().setMag(v.mag() * magFactor * (1 + noisiness * noiseFactor));
      curveVertex(w.x, w.y);
    }

    endShape(CLOSE);
    popMatrix();
  }
  
    public void paintStyled(float x, float y, float noisiness, String mode, float magFactor) {
    pushMatrix();
    pushStyle();
    translate(x, y);

    beginShape();
    strokeCap(PROJECT);

    for (PVector v : mutatedVertices) {
      if (mode == "fill") fill(getColor(v));
      else if (mode == "stroke") {strokeWeight(radius / 5); stroke(getColor(v)); }
      
      PVector w = v.copy().setMag(v.mag() * magFactor);
      curveVertex(w.x, w.y);
    }

    endShape(CLOSE);
    popStyle();
    popMatrix();
  }
  
  
  public void paint(float x, float y, float noisiness, String mode) {
    paint(x, y, noisiness, mode, 1);
  }
  
  public void paint(PVector o, float noisiness, String mode, float magFactor) {
    paint(o.x, o.y, noisiness, mode, magFactor);
  }
  
  public void paint(PVector o, float noisiness, String mode) {
    paint(o.x, o.y, noisiness, mode);
  }
}
