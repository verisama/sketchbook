import java.util.Arrays;
import com.cage.colorharmony.*;

enum PaletteLerpMode   { LINEAR, LOG, EXP };
enum PaletteCreateMode { MONOCHROMATIC, ANALOGOUS, COMPLEMENTARY, TRIADS };

class Palette {
  color[] colors;
  color foreground;
  color background;
  PaletteLerpMode mode;
  
  // CREATE A COLORHARMONY INSTANCE (DEFAULT CONSTRUCTOR)
  ColorHarmony colorHarmony;
  
  /**
  * Constructors
  */
  
public Palette (String colors) {
    String[] hexColors = colors.split("-");
    this.colors = new color[hexColors.length];
    
    for (int i = 0; i < hexColors.length; i++) { this.colors[i] = unhex("FF" + hexColors[i]); }
    
    this.mode = PaletteLerpMode.LINEAR;
  }
  
  public Palette (color[] colors) {
    this.colors = colors;
    this.mode = PaletteLerpMode.LINEAR;
  }
  
  public Palette (String colors, PaletteLerpMode mode) {
    String[] hexColors = colors.split("-");
    this.colors = new color[hexColors.length];
    
    for (int i = 0; i < hexColors.length; i++) { this.colors[i] = unhex("FF" + hexColors[i]); }
    
    this.mode = mode;
  }
  
  public Palette (color[] colors, PaletteLerpMode mode) {
    this.colors = colors;
    this.mode = mode;
  }
  
  
  /* Palette from 1 color using Color Harmony library */
  public Palette (PApplet context, String c, int size, PaletteCreateMode mode) {
    this.colorHarmony = new ColorHarmony(context);
    this.colors = new color[size];
    switch (mode) {
      case MONOCHROMATIC:
        this.colors = colorHarmony.Monochromatic(c);
        break;
      case ANALOGOUS:
        this.colors = colorHarmony.Analogous(c);
        break;
      case COMPLEMENTARY:
        this.colors = colorHarmony.Complementary(c);
        break;
      case TRIADS:
        this.colors = colorHarmony.Triads(c);
        break;
    }
    this.mode = PaletteLerpMode.LINEAR;
  }
  
  public Palette (PApplet context, color c, int size, PaletteCreateMode mode) {
    this.colorHarmony = new ColorHarmony(context);
    this.colors = new color[size];
    switch (mode) {
      case MONOCHROMATIC:
        this.colors = colorHarmony.Monochromatic(colorHarmony.P52Hex(c));
        break;
      case ANALOGOUS:
        this.colors = colorHarmony.Analogous(colorHarmony.P52Hex(c));
        break;
      case COMPLEMENTARY:
        this.colors = colorHarmony.Complementary(colorHarmony.P52Hex(c));
        break;
      case TRIADS:
        this.colors = colorHarmony.Triads(colorHarmony.P52Hex(c));
        break;
    }
    this.colors = colorHarmony.Monochromatic();
    this.mode = PaletteLerpMode.LINEAR;
  }
  
  
  public Palette (PApplet context, color c, int size, PaletteCreateMode mode, PaletteLerpMode lerpMode) {
    this.colorHarmony = new ColorHarmony(context);
    this.colors = new color[size];
    switch (mode) {
      case MONOCHROMATIC:
        this.colors = colorHarmony.Monochromatic(colorHarmony.P52Hex(c));
        break;
      case ANALOGOUS:
        this.colors = colorHarmony.Analogous(colorHarmony.P52Hex(c));
        break;
      case COMPLEMENTARY:
        this.colors = colorHarmony.Complementary(colorHarmony.P52Hex(c));
        break;
      case TRIADS:
        this.colors = colorHarmony.Triads(colorHarmony.P52Hex(c));
        break;
    }
    this.colors = colorHarmony.Monochromatic();
    this.mode = lerpMode;
  }
  
  
  
  /**
  * Get Color Methods
  */
  
  color getColor () { return getColor(noise(frameCount, random(1))); }
  
  color getColor(float v) { return getColor(v, mode); }
  
  color getColor(float v, PaletteLerpMode mode) { return getColor(v, colors.length, mode); }
  
  color getColor(float v, float max, PaletteLerpMode mode) {
    v = constrainValue(v, max, mode);
    
    float c1 = floor(v * colors.length);
    float c2 = ceil(v * colors.length);
    
    if (c1 == c2) { c2++; }
    
    v = map(v, c1 / colors.length, c2 / colors.length, 0, 1);
    
    if (c2 == colors.length) c2 = 0;
    
    return lerpColor(colors[(int)c1], colors[(int)c2], v);
  }
  
  
  float constrainValue (float v, float max, PaletteLerpMode mode) {
    v = abs(v);
    
    if (v > 1) { v = (v % max) / max; }
    
    switch (mode) {
      case LINEAR: break;
      case EXP: v = map(exp(v), exp(0), exp(1), 0, 1); break;
      case LOG: v = map(log(v * 10), log(1), log(10), 0, 1); break;
    }
    
    v = v % 1.0;
    
    return max(0, v);
  }
  
  color getColorDiscrete(float v) { return getColorDiscrete(v, mode); }
  
  color getColorDiscrete(float v, int size) { return getColorDiscrete(v, size, mode); }
  
  color getColorDiscrete(float v, PaletteLerpMode mode) { return getColorDiscrete(v, colors.length, mode); }
  
  color getColorDiscrete(float v, int size, PaletteLerpMode mode) {
    return getColor(floor(v * size) / (float)size, mode);
  }
  

  /**
  * Special Color Methods
  */
  
  void setForeground(color c) { this.foreground = c; }
  void setBackground(color c) { this.background = c; }
  color fg() { return foreground; }
  color bg() { return background; }

  color lightest() { color[] cs = colors.clone(); Arrays.sort(cs); return cs[cs.length - 1]; }
  color darkest()  { color[] cs = colors.clone(); Arrays.sort(cs); return cs[0]; }
  
  void drawBackground () { drawBackground(255); }
  void drawBackground (float alpha) {
    pushStyle();
    noStroke();
    fill(background, alpha);
    rect(0, 0, width, height);
    popStyle();
  }
 
  /**
  * Debug Methods
  */
  
  void draw () {
    pushStyle();
    noFill();
    strokeWeight(1);
    for (int x = 0; x < width; x++) {
      float v = (float) x / width;
      color c = getColorDiscrete(v, 36);
      stroke(c);
      line(x, 0, x, height);
    }
    popStyle();
  }
}
