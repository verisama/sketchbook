class Winds {
 int iterations = 10;
 Wind[] winds = new Wind[iterations];
 
 public Winds () {
  //  for (int i=0; i < iterations; i++) {
  //     float dirX = random(1);
  //     float dirY = randomGaussian();
      
  //     float x = 0;
  //     float y = random(height);
      
  //     if (i < iterations / 2) {
  //       x = width;
  //       dirX = -dirX;
  //     }
      
  //     winds[i] = new Wind(new PVector(x, y), new PVector(dirX, dirY));
  //   }
   for (int i=0; i < iterations; i++) {
      winds[i] = new Wind(new PVector(width / 2, height / 2), PVector.fromAngle(PI * i / iterations));
    }
 }
 
 void draw() {
   for (Wind wind : winds) {
     wind.draw();
   }
 }
}
