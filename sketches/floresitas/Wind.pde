class Wind {
 float iterations = 3;
 
 PVector origin;
 PVector direction;
 float angle;
 
 float a = 0;
 float amplitude = 1;
 float inc = TWO_PI/25.0;
    
 float x, y, size;

 public Wind (PVector origin, PVector direction) {
   this.origin = origin;
   this.direction = direction.normalize();
   this.angle = direction.heading();

  this.x = origin.x;
  this.y = origin.y;
  this.size = 2;
 }
 
  void draw() {
    for (int i=0; i < iterations; i++) { 
      size += randomGaussian() * 2;
      
      PVector aaa = PVector.random2D();
      
      pushMatrix();
      fill(aaa.x * 128, aaa.y * 255, size, 30);
      noStroke();
      translate(x + aaa.x * size, y + aaa.y * size);
      ellipse(0, 0, size, size);
      popMatrix();

      y += sin(a+=inc) * amplitude;
    }
  }
}
