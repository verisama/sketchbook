Winds winds;

float FACTOR = .5;

void settings() {
  size((int)(1920 * FACTOR), (int)(1080 * FACTOR));
}


void setup () {
  blendMode(DIFFERENCE);

  winds = new Winds();
}


void draw () {
  winds.draw();
}


void keyPressed () {
  winds.draw();
}
