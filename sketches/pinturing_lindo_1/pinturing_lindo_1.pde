
AlphaDancer[] alphaDancers;
FlowerDancer[] flowerDancers;

int MODE = 1;
int BG_OPACITY = 1;
int ALPHA_MIN = 60;
int ALPHA_MAX = 255; 
int SPEED_MIN = 1;
int SPEED_MAX = 4;

int N_ALPHA_DANCERS = 25;
int N_FLOWER_DANCERS = 1;

void setup () {
  blendMode(DIFFERENCE);

  fullScreen();
  // size(600, 600);
  
  init();
  palette.drawBackground(255);
  
  prepare();
  
  /** STOPPED MODE */
  if (MODE == 0) {
    palette.draw();
  }
  /** REGULAR MODE */
  else if (MODE == 1) {
    
  }
  /** DEBUG MODE */
  else if (MODE == 9) {
    palette.draw();
  }
}


void draw () {
  /** STOPPED MODE */
  if (MODE == 0) {
    return;
  }
  /** REGULAR MODE */
  else if (MODE == 1) {
    
    //palette.drawBackground(BG_OPACITY);
    updateDancers();
  }
  /** DEBUG MODE */
  else if (MODE == 2) {
    palette.draw();
  }
}

void updateDancers () {
  for (int i = 0; i < alphaDancers.length; i++) {
    alphaDancers[i].setAlpha(map(mouseY, 0, height, ALPHA_MIN, ALPHA_MAX));
    alphaDancers[i].setSpeed(map(mouseX, 0, width, SPEED_MIN, SPEED_MAX));
    alphaDancers[i].update();
    alphaDancers[i].draw(3, palette.getColor((float) i / alphaDancers.length * (0.5 + alphaDancers[i].direction.x + alphaDancers[i].direction.y)));
}
  
  for (int i = 0; i < flowerDancers.length; i++) {
    flowerDancers[i].setAlpha(map(mouseY, height, 0, ALPHA_MIN, ALPHA_MAX));
    flowerDancers[i].setSpeed(map(mouseX, width, 0, SPEED_MIN, SPEED_MAX));
    flowerDancers[i].update();
    flowerDancers[i].draw(2, palette.getColor((float) i / flowerDancers.length));
  }
}

void prepare() {
  PVector center = new PVector(width / 2, height / 2);
  
  alphaDancers = new AlphaDancer[N_ALPHA_DANCERS];
  for (int i = 0; i < alphaDancers.length; i++) {
    float angle = radians(360.0 / (float) alphaDancers.length * i);
    float size = random(10, 80);
    float speed = random(1, 5) / 10;
    PVector direction = new PVector(cos(angle), sin(angle));
    
    alphaDancers[i] = new AlphaDancer(center, direction, size, speed);
  }
  
  
  flowerDancers = new FlowerDancer[N_FLOWER_DANCERS];
  for (int i = 0; i < flowerDancers.length; i++) {
    
    float angle = radians(360.0 / (float) flowerDancers.length * i);
    float size = random(height / 10, height / 2);
    float speed = random(5);
    PVector direction = new PVector(cos(angle), sin(angle));
    
    PVector origin = new PVector(random(size * 2, width - size * 2), random(size * 2, height - size * 2));
 
    if (i > 0) {
      float x1 = flowerDancers[i-1].transform.position.x;
      float y1 = flowerDancers[i-1].transform.position.y;
        
      while ((origin.x != x1 || origin.y != y1) && dist(origin.x, origin.y, x1, y1) < 0) {
        origin = new PVector(random(size * 2, width - size * 2), random(size * 2, height - size * 2));
      }
    }
    
    int petals = i * 3 + 5; // ignored ?
    flowerDancers[i] = new FlowerDancer(origin, direction, size, speed, petals);
  }
}
