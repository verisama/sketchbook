import dawesometoolkit.*;
DawesomeToolkit dawesome;

Palette palette;

void init () {
  int chosenPalette = 2;
  String[] palettes = new String[]{
    "63372c-c97d60-F9DBBD-f2e5d7-DB5461",
    "a1e8af-94c595-747c92-372772-3a2449",
    "42033d-680e4b-7c238c-854798-7c72a0"
  };
  
  colorMode(HSB, 360, 100, 100);
  color c = color(random(360), random(50, 90), random(70, 100));
  colorMode(RGB, 255, 255, 255, 255);
  
  palette = new Palette(palettes[chosenPalette], PaletteLerpMode.EXP); // generated with https://coolors.co/app
  palette = new Palette(this, c, 16, PaletteCreateMode.MONOCHROMATIC);
  palette.setBackground(#101010); //262322
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}
