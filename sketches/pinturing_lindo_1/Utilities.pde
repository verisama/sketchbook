final static float PHI = (sqrt(5)+1)/2;

boolean chance (int outOfAHundred) {
  return random(100) < outOfAHundred;
}

void saveTheFrame () {
  String name = String.format("%02d%02d%02d%02d%02d%02d", month(), day(), hour(), minute(), second(), millis());
  saveFrame("out_" + name + ".png");
}
