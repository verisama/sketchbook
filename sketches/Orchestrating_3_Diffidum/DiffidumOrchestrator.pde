class DiffidumOrchestrator extends Orchestrator {
 
  public DiffidumOrchestrator (PApplet context, Media media) {
    super(context, media);
    
    this.dynamicLevel = true;
  }
  
  public void setup () {
    colorMode(HSB, 360, 100, 100, 100);
    
    background(30, 100, 100, 100); // DEBUG
  }
  
  public void onBeat () {
    super.onBeat();
    
    background(0);
  }
  
  public void onLevelPeak () {
    super.onLevelPeak();
    
  }
  
  public void onLevelMid () {
    super.onLevelMid();
    
  }
  
  public void onLevelLow () {
    super.onLevelLow();
    
  }
  
  
  public void onKick () {
    super.onKick();
    
    background(#0000ff);
  }
  public void onSnare () {
    super.onSnare();
    
    background(#00ff00);
  }
  public void onHat () {
    super.onHat();
    
    background(#ff0000);
  }
  
  
  public void always () {
    super.always();
    
  }
  
 

}
