class Media {
  // Using just the path of this sketch to demonstrate,
  // but you can list any directory you like.
  String path;
  String format;
  
  ArrayList<File> files;
  
  Minim minim;
  AudioPlayer audio;
  FFT fft;
  BeatDetect beat;
  
  public Media (PApplet context, String format) {
    this.path = join(new String[]{sketchPath(), "data"}, "/");
    this.format = format;
    this.files = new ArrayList<File>();
    
    this.minim = new Minim(context);
    
    this.read();
  }
  
  public AudioPlayer loadAudio (String name) {
    this.audio = minim.loadFile(name, 1024);
    this.fft = new FFT(audio.bufferSize(), audio.sampleRate());
    this.beat = new BeatDetect(audio.bufferSize(), audio.sampleRate());
    beat.detectMode(BeatDetect.FREQ_ENERGY);
    beat.setSensitivity(300);
    
    return this.audio;
  }

  private void read () {
    File[] allFiles = listFiles(path);

    for (File f : allFiles) {
      if (!f.isDirectory() && match(f.getName(), format) != null) {
        files.add(f);
      }
    }  
  }
  
  public String getFile(int i) {
    File f = files.get(i % files.size());
    
    return f.getName();
  }
  
  public String getRandomFile() {
    int i = (int) random(files.size());
    
    return getFile(i);
  }
  
  public AudioPlayer getSong () {
    return audio;
  }
  
  public void play() {
    audio.play();
  }
  
  public void pause() {
    audio.pause();
  }
  
  public void stop() {
    audio.close();
  }
  
  public float position () {
    return song.position();
  }
  
  public float length () {
    return song.length();
  }
  
  public float level() {
    return song.mix.level();
  }
  
  public boolean isPlaying() {
    return audio.isPlaying();
  }
  
  public boolean isBeating() {
    return beat.isOnset();
  }
}
