import processing.sound.*;

public static final float SCALE = 80;
public static final int W = (int)SCALE * 16;
public static final int H = (int)SCALE * 9;

public static final int PADDING = W/20; //(px)

BlobbyOrchestrator orchestrator;
Media media;
SoundFile song;

boolean DEBUG = false;

void settings () {
  size(W, H);
  smooth();
}

void setup() {
  init();

  blendMode(DIFFERENCE);
  
  media = new Media(this, "mp3");
  song = media.loadAudio(media.getFile(2));
  song.jump(50); // seconds
  song.pause();
  orchestrator = new BlobbyOrchestrator(this, media);

  background(0);

  orchestrator.setup();
  orchestrator.play();
}

void draw() {
  orchestrator.orchestrate();
  
  texturize();
}

void texturize() {
  noFill();
  int lines = 200;
  for (int i = 0; i < lines; i++) {
    // Scratches
    stroke(255);
    float mag = 2 * (noise(i) * 9 + 1);
    strokeWeight(1 / mag);
    PVector direction = PVector.random2D().setMag(mag);
    PVector origin = new PVector(random(width), random(height));
    line(origin.x, origin.y, origin.x + direction.x, origin.y + direction.y); 

    // Scanlines
    if (i % 10 == 0){
      strokeWeight(50);
      stroke(noise(i, frameCount % 20)*255, 1);
      float y = i / (float)lines * height;
      line(0, y, width, y);
    }
    
  }
  
  for (int i = 0; i < 200; i++) {
    strokeWeight(random(3, 5));
    if (random(60) > 55) { strokeWeight(20);}
    point(random(width), random(height));
  }
}
