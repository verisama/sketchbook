PShader shader;
color[] palette = new color[]{ #333333, #32faab, #faab32, #ab32fa };
color getColor (int i) { return palette[i]; }
color getColor ()      { return palette[int(random(1, palette.length))]; }

boolean SAVE = false;

void setup () {
  size(512, 512, P3D);
  colorMode(HSB, 360, 100, 100, 100);
  background(getColor(0));
  shader = loadShader("wrap.glsl");
}

void alphaBackground (color c, float a) {
  pushStyle();
  noStroke();
  fill(c, a);
  rect(0, 0, width, height);
  popStyle();
}

void draw () {
  generate();
  filter(shader);

// Channels
/*
  shader.set("rbias", 0.0, 0.0);
  shader.set("gbias", map(mouseY, 0, height, -0.2, 0.2), 0.0);
  shader.set("bbias", 0.0, 0.0);
  shader.set("rmult", map(mouseX, 0, width, 0.8, 1.5), 1.0);
  shader.set("gmult", 1.0, 1.0);
  shader.set("bmult", 1.0, 1.0);
  */
  // Wrap
  shader.set("radius", map(mouseX, 0, width, 0, 2));
  shader.set("radTwist", map(mouseY, 0, height, 1.0, 5));

}

void generate () {
  alphaBackground(getColor(0), 30);
 
  noStroke();
  pushMatrix();
  translate(width / 2, height / 2);
  
  for (int i=0; i<300; i++) {
    
    fill(random(300, 360), random(30, 80), random(60, 100));
    
    float posX = randomGaussian() * width / 2;
    float posY = randomGaussian() * height / 2;
    
    float size = random(30, height / 2);
    ellipse(random(posX), random(posY), size, size);
  }
  popMatrix();
  
  if (SAVE) saveFrame("patches_####.tga");
}

void keyPressed () {
  if (key == 'G' || key == 'g') {
    generate();
  }
}
