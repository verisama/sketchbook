import dawesometoolkit.*;

DawesomeToolkit dawesome;

// generada amb https://coolors.co/app
color[] palette = new color[] { #3C3C3B, #1446A0, #DB3069, #F5D547, #2F3061 };

Element1[] elements1;

int numberElements1 = 18;

void setup () {
  background(palette[0]);
  size(600, 600);

  dawesome = new DawesomeToolkit(this);
  // Turn on the saveFrame capture feature
  // Creates a unique time based filename and has a 500ms debounce built-in.
  dawesome.enableLazySave('s',".png");
  
  
  initializeElements();
}

void draw () {
  updateElements1();
}


void updateElements1 () {
  for (int i = 0; i < elements1.length; i++) {
    elements1[i].setAlpha(map(mouseY, 0, height, 0, 255));
    elements1[i].setSpeed(map(mouseX, 0, width, 0, 2));
    elements1[i].update();
    elements1[i].draw(1, getColor());
  }
}

void initializeElements() {
  PVector center = new PVector(width / 2, height / 2);
  
  elements1 = new Element1[numberElements1];
  for (int i = 0; i < elements1.length; i++) {
    float angle = radians(360.0 / elements1.length * i);
    float size = 10; //random(10, 80);
    float speed = 0.1;// random(1, 5) / 10;
    PVector direction = new PVector(cos(angle), sin(angle));
    
    elements1[i] = new Element1(center, direction, size, speed);
  }
}


color getColor () {
  return palette[(int)random(1, palette.length)];
}
