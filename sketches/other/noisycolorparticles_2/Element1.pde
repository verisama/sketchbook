class Element1 extends Particle {
  PVector[] vertices;

  float strokeWeight = 4;
  float alpha = 10;

  public Element1 (PVector origin, PVector direction, float size, float speed) {
    super(origin, direction, size, speed);
    
    rotation = radians(random(360));
    setVertices(radians(rotation + 0), radians(rotation + 120), radians(rotation + 240));
  }
  
  
  void setVertices (float angle1, float angle2, float angle3) {
    vertices = new PVector[] {
      new PVector(cos(angle1) * size, sin(angle1) * size),
      new PVector(cos(angle2) * size, sin(angle2) * size),
      new PVector(cos(angle3) * size, sin(angle3) * size)
    };
  }
  
  
  void update () {
    super.update();
    // updateDirection();
    updatePosition();
    updateRotation();
  }
  
  void updateDirection () {
    direction.x += randomGaussian() / 100;
    direction.y = direction.x * 100 % 10 == 0 ? 1 : -1;
  }
  
  void updatePosition () {
    centre.add(direction);
    
    for (PVector v : vertices) {
      v.add(direction.copy().mult(speed));
    }
    
    if (outOfBounds()) {
      invertDirection();
    }
  }
  
  void updateRotation () {
    rotation += noise(centre.x, centre.y);
  }
  
  void updateSize () {
    size -= speed * direction.heading() / 10;
    setVertices(radians(rotation + 0), radians(rotation + 120), radians(rotation + 240));
  }
  
  void setSpeed (float v) { speed = v; }
  void setAlpha (float v) { alpha = v; }
  
  boolean outOfBounds () {
    return centre.x > width || centre.y > height || centre.x < 0 || centre.y < 0;
  }
  
  void invertDirection () {
    direction.x = -direction.x;
    direction.y = -direction.y;
  }
  
  void draw1 (color c) {
    pushStyle();
    noFill();
    strokeWeight(strokeWeight);
    stroke(c, alpha);
    
    triangle(vertices[0].x, vertices[0].y,
             vertices[1].x, vertices[1].y,
             vertices[2].x, vertices[2].y);
    
    
    popStyle();
  }
  
  void draw2 (color c) {
  
  }
  
  void draw3 (color c) {
  
  }
}
