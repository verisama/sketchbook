class Particle {
  PVector centre;
  PVector direction;
  
  float rotation;
  float size;
  float speed;
  
  boolean dead;

  public Particle (PVector origin, PVector direction, float size, float speed) {
    this.centre = origin;
    this.direction = direction;
    this.size = size;
    this.speed = speed;
    this.rotation = 0;
    this.dead = false;
  }  
  
  
  void update () {
    if (dead) return;
    
  }
  
  
  void draw (int mode, color c) {
    if (dead) return;
    
    pushMatrix();
    translate(centre.x, centre.y);
    rotate(rotation);
    
    switch (mode) {
      case 1:
        draw1(c);
        break;
      case 2:
        draw2(c);
        break;
      case 3:
        draw3(c);
        break;
    }
    
    popMatrix();
  }
  
  void draw1 (color c) {

  }
  
  void draw2 (color c) {
  
  }
  
  void draw3 (color c) {
  
  }
}
