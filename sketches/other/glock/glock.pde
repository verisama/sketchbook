color[] palette = new color[]{ #333333, #aaaaaa, #7E1424, #F2E982, #E5B400, #00D0E5 };

void setup () {
  size(512, 512);
  colorMode(HSB, 360, 100, 100, 100);
  generate();
}

void draw () {
}

void generate () {
  noiseSeed((long)(random(width)));
  background(getColor(0)); 
  drawEllipse();

  texturizeAll();
}

void drawEllipse () {
  color c = getColor(1);
  float size = height / PHI;
  pushMatrix();
  pushStyle();

  translate(width / 2, height / 2);

  // splasshhh
  for (int i = 0; i < 8 - random(7); i++) {
    
    noFill();
    stroke(getColor(2), random(20, 80));
    strokeWeight(random(10, 100));
    arc(random(height/2), random(height / 2 ), width, random(height / 2 ), random(height/2), random(height / 2 ));
  }
  
  fill(c, 30);
  stroke(0);
  strokeJoin(ROUND);
  strokeWeight(8);

  drawTriangle(size);
  drawTriangle(size);
  drawTriangle(size);

  strokeWeight(5);
  drawTriangle(size / 20);

  strokeWeight(5);
  drawTriangle(size / 20);

  strokeWeight(3);
  drawTriangle(size / 50);


  fill(#00D0E5, 30);
  stroke(#00D0E5, 60);
  strokeWeight(8);
  drawTriangle(size * noise(second() / 100));
  drawTriangle(size * noise(second() / 100));
  
  fill(getColor(2), 90);
  stroke(255, 60);
  strokeWeight(randomGaussian() * 15 + 50);
  float r = random(width / 5, width);
  ellipse(0, height / 2, r, r);



  noFill();
  stroke(50, 50);

  for (int i = 0; i < 8 - random(7); i++) { 
    strokeWeight(random(height / 50, height / 30));
    float h = random(height) / 2;
    line(-width / 2, h, width / 2, h + 80);
    // ellipse(0, 0, size, size);
  }

  drawWave();
  drawWave();
  drawWave();


  drawWave();
  drawWave();
  drawWave();


  // fill(getColor(3), 90);g
  // stroke(getColor(3), 60);
  strokeWeight(randomGaussian() * 15 + 50);
  float r2 = random(10, 50);
  ellipse(randomGaussian() * width, height / 2 + randomGaussian() * 80, r2, r2);





  popStyle();
  popMatrix();
}

void drawWave () {
  float size = width / 4;
  pushMatrix();
  for (float x = -size; x < size; x++) {
    translate(randomGaussian() * width / 4, randomGaussian() * width / 4); // scatters
    stroke(0, 20);
    strokeWeight(random(size / 20, size / 10));
    rotate(HALF_PI * noise(size * 0.2, -size * 0.2));
    point(x, x % 5);
  }
  popMatrix();
}

void drawTriangle(float size) {
  float x1 = randomGaussian() * (width / 2);
  float y1 = randomGaussian() * (height / 2);

  float x2 = x1 - random(size * 3);
  float y2 = y1 + random(size * 3);

  float x3 = x1 + random(size * 3);
  float y3 = y1 + random(size * 3);

  pushMatrix();
  rotate(HALF_PI * randomGaussian());
  triangle(x1, y1, x2, y2, x3, y3);
  triangle(x1, y1 - size, x2 - size, y2  + size, x3 + size, y3  + size);
  popMatrix();
}

void keyPressed () {
  if (key == 'g' || key =='G') {
    generate();
  } else if (key == 's' || key == 'S') {
    saveTheFrame();
  } else if (key == 't' || key == 'T') {
    texturize = !texturize;
  }
}
