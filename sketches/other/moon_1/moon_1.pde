color[] palette = new color[]{ #333333, #aaaaaa };
color getColor (int i) { return palette[i]; }
color getColor ()      { return palette[int(random(1, palette.length))]; }
float rg (float min, float max) { return min + randomGaussian() * (max - min); }
float rg (float max) { return rg(0, max); }
float rg () { return rg(1); }
float rga (float min, float max) { return min + (abs(randomGaussian()) * (max - min)); }
float rga (float max) { return rga(0, max); }
float rga () { return rga(1); }
int   rgi (float min, float max) { return int(rga(min, max)); }
int   rgi (float max) { return rgi(0, max); }

final static float PHI = (sqrt(5)+1)/2;

boolean texturize = true;

void setup () {
  size(512, 512);
  colorMode(HSB, 360, 100, 100, 100);
  generate();
}

void draw () {}

void generate () {
  background(getColor(0));
  drawEllipses();
  
  if (texturize) {
    noiseTexture(60, 50);
    noiseTexture(30, 30);
    scratchesTexture(20);
    grainTexture(10, false);
  }
}

void drawEllipses () {
  float iterations = 8;
  boolean placedSatellite = false;

  pushStyle();
  pushMatrix();
  translate(width / 2, height / 2);
  for (int i = 0; i < iterations; i++) {
    float delta = PHI / i;
    float size  = delta * height / 2;
    color originalColor = getColor(1); 
    color c = color(hue(originalColor), saturation(originalColor), brightness(originalColor), 100 - 100 * delta);
    
    boolean filled = chance(50);
    if (filled) {
      noStroke();
      fill(c);
    }
    else {
      noFill();
      stroke(c);
      strokeWeight(2);
    }
    
    ellipse(0, 0, size, size);
    
    if (!filled && !placedSatellite) {
      float satelliteSize = size / 10;
      noStroke();
      fill(hue(c), saturation(c), brightness(c), 100);
      float a = random(TWO_PI);
      float x = cos(a) * size / 2;
      float y = sin(a) * size / 2;
      ellipse(x, y, satelliteSize, satelliteSize);
      
      placedSatellite = true;
    }
  }
  popStyle();
  popMatrix();
}

void alphaBackground (color c, float a) {
  pushStyle();
  noStroke();
  fill(c, a);
  rect(0, 0, width, height);
  popStyle();
}

/**
 * Another way to adjust the character of the resulting sequence is
 * the scale of the input coordinates. As the function works within
 * an infinite space, the value of the coordinates doesn't matter as
 * such; only the distance between successive coordinates is
 * important (such as when using noise() within a loop). As a general
 * rule, the smaller the difference between coordinates, the smoother
 * the resulting noise sequence. Steps of 0.005-0.03 work best for
 * most applications, but this will differ depending on use.
 **/
 
void noiseTexture(float alpha, float smoothness) {
  PImage img = createImage(width, height, ARGB);
  img.loadPixels();
  float step = 1;
  float z = random(height);
  for (int x = 0; x < width; x += step) {
    for (int y = 0; y < height; y += step) {
      float noiseValue = noise(x / smoothness, y / smoothness, z / smoothness);
      img.set(x, y, color(0, noiseValue * alpha));
    }
  }
  img.updatePixels();
  image(img, 0, 0);
}

void grainTexture(float alpha, boolean blur) {
  PImage img = createImage(width, height, ARGB);
  img.loadPixels();
  float step = 1;
  float z = random(height);
  for (int x = 0; x < width; x += step) {
    for (int y = 0; y < height; y += step) {
      float noiseValue = noise(x, y, z);
      img.set(x, y, color(0, noiseValue * alpha));
    }
  }
  img.updatePixels();
  
  if (blur) img.filter(BLUR);
  
  image(img, 0, 0);
}

void scratchesTexture(float alpha) {
  float minStep = 10;
  float maxStep = 20;
  
  pushStyle();
  strokeWeight(.8);
  
  for (int x = 0; x < width; x += random(minStep, maxStep)) {
    for (int y = 0; y < height; y += random(minStep, maxStep)) {
      if (chance(85)) {
        float positionX = x;
        float positionY = y;
        float iterations = random(5);
        float scale  = random(200, 300);
        float size   = random(4, 9);
        float offset = random(TWO_PI);
        beginShape(LINES);
       
       for (int i = 0; i < iterations; i++) {
          stroke(lerpColor(get(x, y), color(chance(50) ? 0 : 255), random(.3, .7)), alpha);
          float noiseValue = offset + noise(positionX / scale, positionY / scale) * TWO_PI;
          float noiseX = positionX + cos(noiseValue) * size;
          float noiseY = positionY + sin(noiseValue) * size;
          vertex(positionX, positionY);
          vertex(noiseX, noiseY);
          positionX = noiseX;
          positionY = noiseY;
        }
        endShape();
      }
    }
  }
  
  popStyle();
}

boolean chance (int outOfAHundred) {
  return random(100) < outOfAHundred;
}

void saveTheFrame () {
  String name = String.format("%d%d%d%d%d%d", month(), day(), hour(), minute(), second(), millis());
  saveFrame("render/out_" + name + ".png");
}

void keyPressed () {
  if (key == 'g' || key =='G') {
    generate();
  }
  else if (key == 's' || key == 'S') {
    saveTheFrame();
  }
  else if (key == 't' || key == 'T') {
    texturize = !texturize;
  }
}
