int sketchMode = 1;
int nDancers = 8;

AlphaDancer[] alphaDancers; 

void setup () {
  init();
  size(600, 600);
  
  prepare();
  
  /** STOPPED MODE */
  if (sketchMode == 0) {
    palette.draw();
  }
  /** REGULAR MODE */
  else if (sketchMode == 1) {
    
  }
  /** DEBUG MODE */
  else if (sketchMode == 9) {
    palette.draw();
  }
}


void draw () {
  /** STOPPED MODE */
  if (sketchMode == 0) {
    return;
  }
  /** REGULAR MODE */
  else if (sketchMode == 1) {
    updatealphaDancers();
  }
  /** DEBUG MODE */
  else if (sketchMode == 2) {
    palette.draw();
  }
}


void updatealphaDancers () {
  for (int i = 0; i < alphaDancers.length; i++) {
    alphaDancers[i].setAlpha(map(mouseY, 0, height, 0, 255));
    alphaDancers[i].setSpeed(map(mouseX, 0, width, 0, 2));
    alphaDancers[i].update();
    alphaDancers[i].draw(2, palette.getColor((float) i / alphaDancers.length + second() / 60.0));
  }
}

void prepare() {
  PVector center = new PVector(width / 2, height / 2);
  
  alphaDancers = new AlphaDancer[nDancers];
  for (int i = 0; i < alphaDancers.length; i++) {
    float angle = radians(360.0 / alphaDancers.length * i);
    float size = 10; random(10, 80);
    float speed = 0.1;// random(1, 5) / 10;
    PVector direction = new PVector(cos(angle), sin(angle));
    
    alphaDancers[i] = new AlphaDancer(center, direction, size, speed);
  }
}
