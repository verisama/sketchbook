color[] palette = new color[]{ #333333, #aaaaaa };
Particle[] drops;

PVector min, max;
PVector padding = new PVector(-100, -100);

void setup () {
  size(512, 512);
  colorMode(HSB, 360, 100, 100, 100);
  
  min = new PVector(padding.x, padding.y);
  max = new PVector(width - padding.x, height - padding.y);

  drops = initializeParticles(500);
  
  // generate();
}

void draw () {
  generate();
  saveTheFrame();
  // filter(BLUR);
}

void generate () {
  alphaBackground(getColor(0), 5);
  updateParticles(drops, min, max);
  drawDrops();
  texturizeAll();
}

void drawDrops() {
  pushStyle();
  noStroke();
  
  for (int i = 0; i < drops.length; i++) {
    Particle p = drops[i];
    color c = color(0, 0, 100, 50);  
    fill(c);
    drops[i].draw();
  }
  popStyle();
}

void mousePressed () {
  alphaBackground(color(255), 80);
}

void keyPressed () {
  if (key == 'g' || key =='G') {
    generate();
  }
  else if (key == 's' || key == 'S') {
    saveTheFrame();
  }
  else if (key == 't' || key == 'T') {
    texturize = !texturize;
  }
}
