Particle[] initializeParticles(int n) {
  Particle[] particles = new Particle[n];
  for (int i = 0; i < particles.length; i++) {
    particles[i] = new Particle(random(width), random(height), random(3, 8));
  }
  
  return particles;
}

void updateParticles(Particle[] particles, PVector min, PVector max) {
  for (int i = 0; i < particles.length; i++) {
    particles[i].update(min.x, min.y, max.x, max.y);
  }
}

class Particle extends PVector {
  PVector direction;
  PVector velocity;
  float speed;
  float noiseScale = 100;
  float boostSpeed = 20;
  float minSize = 0;
  
  public Particle (float x, float y, float speed) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    
    this.direction = new PVector();
    this.velocity = new PVector();
  }
  
  public void draw () {
    float size = minSize + (direction.x + direction.y) * 5 / speed + randomGaussian() * frameCount % 3;
    ellipse(x, y, size, size);
  }
  
  public void update (float minX, float minY, float maxX, float maxY) {
    if (!reset(minX, minY, maxX, maxY)) {
      direction.x = .2 * randomGaussian() * noise(x / noiseScale, y / noiseScale, frameCount);
      direction.y = 1;
    
      velocity.set(direction);
      velocity.mult(speed);
    
      add(velocity);
    }
  }
  
  private boolean reset (float minX, float minY, float maxX, float maxY) {
    boolean offX = (x < minX || x > maxX);
    boolean offY = (y < minY || y > maxY);
 
    if (offX || offY) {
      x = random(minX, maxX);
      y = minY + randomGaussian() * 10;
    }
    
    return offX || offY;
  }
}
