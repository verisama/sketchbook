Mass[] masses;
Particle[] particles;
color[] palette = { #002323, #fafafa, #bebebe };

PVector min, max;
PVector padding = new PVector(-100, -100);

int animationSpeed = 50;
int frames = 4000;

void setup () {
  fullScreen();
  frameRate(60);
  smooth(8);
  colorMode(HSB, 360, 100, 100, 100);
  min = new PVector(padding.x, padding.y);
  max = new PVector(width - padding.x, height - padding.y);

  initializeMasses(4);
  initializeParticles(100);
  
  background(palette[0]);
  /*
  for (int i = 0; i < frames; i++) {
    updateParticles();
    drawParticles();
  }
  */
  //texturizeAll();
  
  // saveTheFrame();
}


void draw () {
  // background(palette[0]);
  // drawMasses();

  for (int i = 0; i < animationSpeed; i++) {
    updateParticles();
    drawParticles();
  }
  
//  texturizeAll();
}

/* Mass and Particle initialization, drawing and updating */

void initializeMasses(int n) {
  masses = new Mass[n];
  for (int i = 0; i < n; i++) {
    masses[i] = new Mass(random(width), random(height), random(height / 16, height / 8), randomGaussian() > 0);
  }
}

void initializeParticles(int n) {
  particles = new Particle[n];
  for (int i = 0; i < particles.length; i++) {
    particles[i] = new Particle(random(width), random(height), random(1, 3));
  }
}

void drawMasses() {
  pushStyle();
  noFill();
  strokeWeight(.5);
  stroke(palette[1]);
  for (int i = 0; i < masses.length; i++) {
    masses[i].draw();
  }
  popStyle();
}

void drawParticles() {
  pushStyle();
  noStroke();
  
  for (int i = 0; i < particles.length; i++) {
    Particle p = particles[i];
    float hue1 = map(p.x, 0, width, 0, 180);
    float hue2 = map(p.y, 0, height, 0, 180);
    float hueOffset = noise(p.x / 50, p.y / 50) * randomGaussian() * 30;
    color c = color(hue1 + hue2 + hueOffset, 75, p.speed * 50, 20);  
    fill(c);
    particles[i].draw();
  }
  popStyle();
}

void updateParticles() {
  for (int i = 0; i < particles.length; i++) {
    particles[i].update(min.x, min.y, max.x, max.y);
  }
}

void keyPressed () {
  if (key == 's' || key == 'S') {
    saveTheFrame();
  }
  else if (key == 't' || key == 'T') {
    texturize = !texturize;
  }
}
