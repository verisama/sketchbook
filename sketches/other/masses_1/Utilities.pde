color getColor (int i) { return palette[i]; }
color getColor ()      { return palette[int(random(1, palette.length))]; }
float rg (float min, float max) { return min + randomGaussian() * (max - min); }
float rg (float max) { return rg(0, max); }
float rg () { return rg(1); }
float rga (float min, float max) { return min + (abs(randomGaussian()) * (max - min)); }
float rga (float max) { return rga(0, max); }
float rga () { return rga(1); }
int   rgi (float min, float max) { return int(rga(min, max)); }
int   rgi (float max) { return rgi(0, max); }

final static float PHI = (sqrt(5)+1)/2;

void alphaBackground (color c, float a) {
  pushStyle();
  noStroke();
  fill(c, a);
  rect(0, 0, width, height);
  popStyle();
}

boolean chance (int outOfAHundred) {
  return random(100) < outOfAHundred;
}

void saveTheFrame () {
  String name = String.format("%02d%02d%02d%02d%02d%02d", month(), day(), hour(), minute(), second(), millis());
  saveFrame("render/out_" + name + ".png");
}
