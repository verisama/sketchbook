class Particle extends PVector {
  PVector direction;
  PVector velocity;
  float speed;
  float noiseScale = 100;
  float boostSpeed = 20;
  float minSize = 5;
  
  public Particle (float x, float y, float speed) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    
    this.direction = new PVector();
    this.velocity = new PVector();
  }
  
  public void draw () {
    float size = minSize + (direction.x + direction.y) * 5 / speed + randomGaussian() * frameCount % 3;
    ellipse(x, y, size, size);
  }
  
  public void update (float minX, float minY, float maxX, float maxY) {
    if (!bounce(minX, minY, maxX, maxY)) {
      float angle = noise(x / noiseScale, y / noiseScale) * TWO_PI;
      direction.x = cos(angle);
      direction.y = sin(angle);
    
      velocity.set(direction);
      velocity.mult(speed);
    
      add(velocity);
    }
  }
  
  private boolean bounce (float minX, float minY, float maxX, float maxY) {
    boolean offX = (x < minX || x > maxX);
    boolean offY = (y < minY || y > maxY);
    if (offX) x = random(minX, maxX);
    if (offY) y = random(minY, maxY);

    if (offX || offY) {
      velocity.set(direction);
      velocity.mult(speed * boostSpeed);
      add(velocity);
    }
    
    return offX || offY;
  }
}
