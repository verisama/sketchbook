class Mass {
  float x, y, size;
  boolean gravity;
  
  public Mass (float x, float y, float size, boolean gravity) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.gravity = gravity;
  }
  
  public void draw () {
    ellipse(x, y, size, size);
  }
}
