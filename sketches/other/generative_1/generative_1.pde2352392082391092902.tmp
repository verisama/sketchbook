// PShader shader;
color[] palette = new color[]{ #333333, #32faab, #faab32, #ab32fa };
color getColor (int i) { return palette[i]; }
color getColor ()      { return palette[int(random(1, palette.length))]; }
float rg (float min, float max) { return min + randomGaussian() * (max - min);}
float rg (float max) { return rg(0, max);}
float rg () { return rg(1);}
float rga (float min, float max) { return min + (abs(randomGaussian()) * (max - min));}
float rga (float max) { return rga(0, max);}
float rga () { return rga(1);}
int rgi (float min, float max) { return int(rga(min, max)); }
int rgi (float max) { return rgi(0, max); }

float globalScale;

void setup () {
  size(512, 512, P3D);
  colorMode(HSB, 360, 100, 100, 100);
  background(getColor(0));
  globalScale = height / 64;
  // shader = loadShader("bevel.glsl");
}

void alphaBackground (color c, float a) {
  pushStyle();
  noStroke();
  fill(c, a);
  rect(0, 0, width, height);
  popStyle();
}

void draw () {
  //filter(shader);
}

void generate () {
  alphaBackground(getColor(0), 30);
  drawTexture();
  
  pushMatrix();
  translate(width / 2, height / 2);
  rotate(random(TWO_PI));
  
  drawPlant( width / 4,  height / 4, 0.5, rgi(3, 8) * 2);
  drawPlant(-width / 4,  height / 4, 0.5, rgi(3, 8) * 2);
  drawPlant( width / 4, -height / 4, 0.5, rgi(3, 8) * 2);
  drawPlant(-width / 4, -height / 4, 0.5, rgi(3, 8) * 2);
  
  drawPlant(0,  height / 4, 0.3, rgi(5, 8) * 2);
  drawPlant(0, -height / 4, 0.3, rgi(5, 8) * 2);
  drawPlant( width / 4, 0,  0.3, rgi(5, 8) * 2);
  drawPlant(-width / 4, 0,  0.3, rgi(5, 8) * 2);
  
  drawPlant(0, 0, 1, rgi(1, 3) * 2 + 1);
  
  popMatrix();
}

void drawLeaf (float scale) {
  scale *= globalScale;
  beginShape();
  vertex(0, 0);
  vertex(-10 * scale, -10 * scale);
  vertex(0, -50 * scale);
  vertex(10 * scale, -10 * scale);
  endShape(CLOSE);
}

void drawPlant (float originX, float originY, float scale, int iterations) {
  pushStyle();
  
  pushMatrix();
  translate(originX, originY);
   
  noStroke();

  color c = getColor();
  float hue = hue(c); 
  float alpha = random(60, 80);
  fill(hue, saturation(c), brightness(c), alpha);
  
  for (int i = 0; i < iterations; i++) {
    rotate(radians(360.0 / iterations));  
    drawLeaf(1.0 / iterations * scale);
  } 
  
  popMatrix();
  popStyle();
}

void drawTexture () {
  int strokeIterations = int(rgi(10, 50) * globalScale);
  int pointIterations  = int(rgi(3,  16) * globalScale);
  
  for (int i = 0; i < strokeIterations; i++) {
    color c = getColor();
    pushStyle();
    pushMatrix();
    
    noFill();
    stroke(c, rga(30));
    
    strokeWeight(1);
    
    for (int j = 0; j < pointIterations; j++) { point(rgi(width), rgi(height)); }
    
    strokeWeight(rg(3));
    
    translate(rgi(width), rgi(height));
    line(0, 0, rg(5), rg(5));
    popMatrix();
    popStyle();
  }
}

void keyPressed () {
  if (key == 'G' || key == 'g') {
    generate();
  }
}
