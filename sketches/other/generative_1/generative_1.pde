// PShader shader;
color[] palette = new color[]{ #333333, #32faab, #faab32, #ab32fa };

float globalScale;

void setup () {
  size(512, 512, P3D);
  colorMode(HSB, 360, 100, 100, 100);
  background(getColor(0));
  globalScale = height / 64;
  // shader = loadShader("bevel.glsl");
}

void alphaBackground (color c, float a) {
  pushStyle();
  noStroke();
  fill(c, a);
  rect(0, 0, width, height);
  popStyle();
}

void draw () {
  //filter(shader);
  generate();
}

void generate () {
  background(getColor(0));
  // alphaBackground(getColor(0), 255);
  drawTexture();
  
  pushMatrix();
  translate(width / 2, height / 2);
  rotate(random(12) * TWO_PI / 12);
  
  float scaleSmall   = height / 5;
  // float scaleSmaller = height / 8;
  
  int sidesSmall   = 6;
  // int sidesSmaller = 12;
  
  drawPlant( scaleSmall,  scaleSmall, 0.5, sidesSmall, 10);
  drawPlant(-scaleSmall,  scaleSmall, 0.5, sidesSmall, 10);
  drawPlant( scaleSmall, -scaleSmall, 0.5, sidesSmall, 10);
  drawPlant(-scaleSmall, -scaleSmall, 0.5, sidesSmall, 10);
  
  /*
  drawPlant(0,  scaleSmaller, 0.3, sidesSmaller, 50);
  drawPlant(0, -scaleSmaller, 0.3, sidesSmaller, 50);
  drawPlant( scaleSmaller, 0, 0.3, sidesSmaller, 50);
  drawPlant(-scaleSmaller, 0, 0.3, sidesSmaller, 50);
  */
  
  drawPlant(0, 0, 1, rgi(1, 2) * 3, 0);
  
  popMatrix();
}

void drawLeaf (float scale) {
  scale *= globalScale;
  beginShape();
  vertex(0, 0);
  vertex(-10 * scale, -10 * scale);
  vertex(0, -50 * scale);
  vertex(10 * scale, -10 * scale);
  endShape(CLOSE);
}

void drawPlant (float originX, float originY, float scale, int iterations, float randomStrength) {
  pushStyle();
  pushMatrix();
  
  translate(
    originX + randomStrength * randomGaussian(),
    originY + randomStrength * randomGaussian()
  );
   
  noStroke();

  color c = getColor();
  float hue = hue(c); 
  float alpha = scale * random(80, 100);
  fill(hue, saturation(c), brightness(c), alpha);
  
  if (rgi(3) == 1) {
    ellipse(0, 0, 50 * scale, 50 * scale);
  }
  else {  
    for (int i = 0; i < iterations; i++) {
      rotate(radians(360.0 / iterations));  
      drawLeaf(1.0 / iterations * scale);
    } 
  }
  
  popMatrix();
  popStyle();
}

void drawTexture () {
  int strokeIterations = int(rgi(10, 50) * globalScale);
  int pointIterations  = int(rgi(3,  16) * globalScale);
  
  for (int i = 0; i < strokeIterations; i++) {
    color c = getColor();
    pushStyle();  
    pushMatrix();
    
    noFill();
    stroke(c, rga(30));
    
    strokeWeight(1);
    
    for (int j = 0; j < pointIterations; j++) { point(rgi(width), rgi(height)); }
    
    strokeWeight(rg(3));
    
    translate(rgi(width), rgi(height));
    line(0, 0, rg(5), rg(5));
    popMatrix();
    popStyle();
  }
}

void keyPressed () {
  if (key == 'G' || key == 'g') {
    generate();
  }
}
