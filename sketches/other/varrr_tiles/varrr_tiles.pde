/**
 *    This example shows how to loop all variations for a given number of elements.
 *    
 *    A "variation" means that elements can be used multiple times (or not at all) and 
 *    that the order is important ( "a,b" is not "b,a", so it's counted ).
 *
 *    given [a, b, c] you'll get:
 *
 *    [a], [b], [c],
 *    [a, a], [a, b], [a, c], [b, a], [b, b], [b, c], [c, a], [c, b], [c, c],
 *    [a, a, a], [a, a, b], .., [c, c, b], [c, c, c]
 *
 *    fjenett 20090305
 */

import de.bezier.math.combinatorics.*;

VariationSet variations;
float drawingWidth = 40, drawingHeight = 40;

float spacing = 10;
float zoom = 1, zoomStep = 0.2;

boolean aBitOfRandom = true;

void setup () {
    size(800, 800);
    smooth();
    init();
    palette.drawBackground();
    
    variations = new VariationSet(3);
    
    // float total = ceil(sqrt(variations.totalAsInt()));
    
    drawingWidth  -= spacing;
    drawingHeight -= spacing;
    
}


void draw () {
  variations.rewind();
  palette.drawBackground();
  scale(zoom);
  int n = 0;
  for (int x = int(spacing / 2); x < width; x += drawingWidth + spacing / 2) {
    for (int y = int(spacing / 2); y < height; y += drawingHeight + spacing / 2) {
      if ( !variations.hasMore() )
          variations.rewind();
      
      int[] v = variations.next();
      int[] nEach = new int[]{ 0, 0, 0 };
      
      for (int i = 0; i < v.length; i++) { drawTile(i, v[i], x, y, n, v, nEach); }
      
      n++;
    }
  }
}


void drawTile (int i, int tileType, float x, float y, int tileNumber, int[] v, int[] nEach) {
  switch (tileType) {
    case 0: drawCircles(i, x, y, tileNumber, v, nEach); nEach[tileType]++; break;
    case 1: drawSquares(i, x, y, tileNumber, v, nEach); nEach[tileType]++; break;
    case 2: drawLines  (i, x, y, tileNumber, v, nEach); nEach[tileType]++; break;
  }
}

void drawCircles (int i, float x, float y, int tileNumber, int[] v, int[] nEach) {
  noFill();
  stroke(palette.getColor((float)tileNumber / variations.totalAsInt()), 255 / v.length * (i+1));
  ellipse(x + drawingWidth/2,
          y + drawingHeight/2,
          drawingWidth  - nEach[0] * drawingWidth  / 5,
          drawingHeight - nEach[0] * drawingHeight / 5);
          
  if (aBitOfRandom) {
    if (noise(x * 0.2, y * 0.2, frameCount / 10) > 0.7) {
      ellipse(x + drawingWidth/2, y + drawingHeight/2, 5, 5);
    }  
  }
}

void drawSquares (int i, float x, float y, int tileNumber, int[] v, int[] nEach) {
  noStroke();
  float alpha = 128 / v.length * (i+1);
  
  fill(palette.getColor((i/3 + (x+y)/(height*2)) / 2), alpha);
  
  rect(x, y,
       drawingWidth  - nEach[1] * drawingWidth  / 5,
       drawingHeight - nEach[1] * drawingHeight / 5);
}

void drawLines (int i, float x, float y, int tileNumber, int[] v, int[] nEach) {
  noFill();
  stroke(palette.getColor((float)tileNumber / variations.totalAsInt()), 255 / v.length * (i+1));
  
  float x0 = x, y0 = y;
  float x1 = drawingWidth  - nEach[2] * drawingWidth / 3;
  float y1 = drawingHeight - nEach[2] * drawingHeight / 3;
  
  if (aBitOfRandom) { x0 += randomGaussian(); y0 += randomGaussian();}                
  if (aBitOfRandom) { x1 += randomGaussian(); y1 += randomGaussian();}
  
  line(x0, y0,      x0 + x1, y0 + y1);
  line(x0, y0 + y1, x0 + x1, y0);
}

void keyPressed () {
  if      (key == '+') { zoom += zoomStep; }
  else if (key == '-') { zoom -= zoomStep; }
}
