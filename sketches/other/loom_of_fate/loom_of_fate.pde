int N_THREADS = 12;

ArrayList<char[]> keys;
boolean[] pressed;

int n = 0;

void setup () {
  size(600, 600);
  
  frameRate(30);
  
  keys = new ArrayList<char[]>();
  keys.add(new char[]{ 'A', 'a' });
  keys.add(new char[]{ 'S', 's' });
  keys.add(new char[]{ 'D', 'd' });
  keys.add(new char[]{ 'F', 'f' });
  keys.add(new char[]{ 'G', 'g' });
  keys.add(new char[]{ 'H', 'h' });
  keys.add(new char[]{ 'J', 'j' });
  keys.add(new char[]{ 'K', 'k' });
  keys.add(new char[]{ 'L', 'l' });
  keys.add(new char[]{ ';' });
  keys.add(new char[]{ '\'' });
  keys.add(new char[]{ '#' });
  
  pressed = new boolean[N_THREADS];
}

void draw () {
  translate(0,n++);
  for (int i = 0; i < pressed.length; i++) {
   if (pressed[i]) {
     float w = width / N_THREADS;
     fill(0);
     stroke(255);
     ellipse(i * w, 0, 8, 8);
   }
  }
}


void resetPressed () {
  for (int i = 0; i < pressed.length; i++) {
   pressed[i] = false;
  }
}

void keyPressed () {
  for (int i = 0; i < keys.size(); i++) {
    char[] all_k = keys.get(i);
    for (char k : all_k) {
      if (key == k) {
       pressed[i] = true; 
      }
    }
  }
}

void keyReleased() {
  for (int i = 0; i < keys.size(); i++) {
    char[] all_k = keys.get(i);
    
    for (char k : all_k) {
      if (key == k) {
       pressed[i] = false; 
      }
    }
    
  }
}
  /*
  int keyIndex = -1;
  if (key >= 'A' && key <= 'Z') {
    keyIndex = key - 'A';
  } else if (key >= 'a' && key <= 'z') {
    keyIndex = key - 'a';
  }
  if (keyIndex == -1) {
    // If it's not a letter key, clear the screen
    background(0);
  } else { 
    // It's a letter key, fill a rectangle
    fill(millis() % 255);
    float x = map(keyIndex, 0, 25, 0, width - rectWidth);
    rect(x, 0, rectWidth, height);
  }
  */
