String base = "TE    ";
String[] endings = new String[]{
  "***", "AMO", "BAR", "BUM", "CUM", "DOY", "FFF", "FOO", "GGG", "LÓN", "JÓN", "MER", "MO.", "MMM", "TA.", "XXX"
};

int lastSelectedEnding = -1;
String selectedEnding;

float charHeight;

void setup () {
  size(400, 400);
  charHeight = height / 10.0;
  generate();
}

void generate () {
  background(0);
  textSize(charHeight);
  text(base, width/2 - charHeight * 0.66 * base.length() / 2, height/2);
  chooseEnding();
}

void chooseEnding () {
  int r;
  do { r = (int)random(endings.length); } while (lastSelectedEnding == r);
  
  selectedEnding = endings[r];
  
  lastSelectedEnding = r; 
}

void draw () {
}

void mousePressed () {
  generate();
}
