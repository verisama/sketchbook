import com.cage.colorharmony.*;
import dawesometoolkit.*;

color[] palette = new color[3];
color base = #dacd98;
int g = 1;

// CREATE COLORHARMONY INSTANCE
// DEFAULTS: minS = 10, maxS = 60, minL = 20, maxL = 60
// SATURATION:  0..100
// LUMOSITY:    0..100
ColorHarmony colorHarmony = new ColorHarmony(this, 70, 100, 0, 50);
DawesomeToolkit dawesome;
ArrayList<PVector> grid;
ArrayList<PVector> layout;

void setup () {
  size(700, 500);
  smooth();
  dawesome = new DawesomeToolkit(this);
  // Turn on the saveFrame capture feature
  // Creates a unique time based filename and has a 500ms debounce built-in.
  dawesome.enableLazySave('s',".png");
  
  
}

void draw () {
  generate(g);
}

void generate_3 () {
  alphaBackground(base, 10);
  translate(width/2,height/2);
  
  layout = dawesome.circularLayout(mouseX, mouseY);
  
  strokeWeight(5);
  
  int i = 0;
  for (PVector p : layout) {
    stroke(palette[i++ % palette.length]);  
    point(p.x, p.y);
  }
}

void generate_2 () {
  palette = colorHarmony.Monochromatic();
  palette = colorHarmony.Monochromatic(base);
  // palette = colorHarmony.Triads(palette[0]);
  grid = dawesome.vogelLayout(mouseX, mouseY);
  
  alphaBackground(base, 10);
  strokeWeight(5);
  
  translate(width/2,height/2);
  
  int i = 0;
  for (PVector p : grid) {
    stroke(palette[i++ % palette.length]);  
    point(p.x, p.y);
  }
}

void generate_1 () {
  if (frameCount % 150 == 0) {
    noStroke();
    palette = colorHarmony.Analogous();
    //palette = colorHarmony.Triads(palette[0]);
    float w = width / palette.length;
    for (int i = 0; i < palette.length; i++) {
      fill(palette[i]);
      rect(i * w, 0, i * w + w, height);
    }
  }
}

void generate(int n) {
  switch (n) {
    case '1': generate_1(); break;
    case '2': generate_2(); break;
    case '3': generate_3(); break;
  }
}

void keyPressed () {
  // generate(key);
  g = int(key);
}
