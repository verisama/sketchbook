import dawesometoolkit.*;
DawesomeToolkit dawesome;


// generada amb https://coolors.co/app
color bg = #3C3C3B;
color[] palette = new color[] { #1446A0, #DB3069, #F5D547, #2F3061 };

Element1[] elements1;

int numberElements1 = 12;

final float MIN_SPEED = .1;
final float MAX_SPEED = .5;
final float MIN_SIZE = 10;
final float MAX_SIZE = 50;

void setup () {
  background(bg);
  size(600, 600);
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png");
  
  initializeElements();
}

void draw () {
  updateElements1();
}


void updateElements1 () {
  for (int i = 0; i < elements1.length; i++) {
    elements1[i].update();
    elements1[i].draw(1);
  }
}

void initializeElements() {
  PVector center = new PVector(width / 2, height / 2);
  
  elements1 = new Element1[numberElements1];
  
  for (int i = 0; i < elements1.length; i++) {
    
    float angle = radians(360.0 / elements1.length * i);
    PVector direction = new PVector(cos(angle), sin(angle));
    
    float size  = random(MIN_SIZE, MAX_SIZE);
    float speed = random(MIN_SPEED, MAX_SPEED);
 
    
    elements1[i] = new Element1(center, direction, size, speed);
  }
}


color getColor () {
  return palette[int(random(palette.length))];
}
