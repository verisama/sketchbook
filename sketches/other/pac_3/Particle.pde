class Particle {
  PVector centre;
  PVector direction;
  
  float rotation;
  float size;
  float speed;
  
  color c;
  
  boolean dead;

  public Particle (PVector origin, PVector direction, float size, float speed) {
    this.centre = origin;
    this.direction = direction;
    this.size = size;
    this.speed = speed;
    this.rotation = 0;
    this.dead = false;
  }  
  
  
  void update () {
    if (dead) return;
  }
  
  
  void draw (int mode) {
    if (dead) return;
    
    pushMatrix();
    translate(centre.x, centre.y);
    rotate(rotation);
    
    switch (mode) {
      case 1:
        draw1();
        break;
      case 2:
        draw2();
        break;
      case 3:
        draw3();
        break;
    }
    
    popMatrix();
  }
  
  void draw1 () {

  }
  
  void draw2 () {
  
  }
  
  void draw3 () {
  
  }
}
