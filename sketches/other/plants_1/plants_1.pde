color[] palette = new color[]{ #333333, #aaaaaa };
color getColor (int i) { return palette[i]; }
color getColor ()      { return palette[int(random(1, palette.length))]; }
float rg (float min, float max) { return min + randomGaussian() * (max - min); }
float rg (float max) { return rg(0, max); }
float rg () { return rg(1); }
float rga (float min, float max) { return min + (abs(randomGaussian()) * (max - min)); }
float rga (float max) { return rga(0, max); }
float rga () { return rga(1); }
int   rgi (float min, float max) { return int(rga(min, max)); }
int   rgi (float max) { return rgi(0, max); }

void setup () {
  size(512, 512);
  colorMode(HSB, 360, 100, 100, 100);
  background(getColor(0));
}

void draw () {}

void generate () {
  drawEllipse();
  noiseTexture(20);
  linesTexture(20);
}

void drawEllipse () {
  pushStyle();
  noStroke();
  fill(getColor(1));
  ellipse(width / 2, height / 2, width / 2, height / 2);
  popStyle();
}

void alphaBackground (color c, float a) {
  pushStyle();
  noStroke();
  fill(c, a);
  rect(0, 0, width, height);
  popStyle();
}

void noiseTexture(float alpha) {
  PImage img = createImage(width, height, ARGB);
  img.loadPixels();
  float step = 1;
  for (int x = 0; x < width; x += step) {
    for (int y = 0; y < height; y += step) {
      float noiseValue = noise(x, y, frameCount);
      img.set(x, y, color(0, noiseValue * alpha));
    }
  }
  img.updatePixels();
  image(img, 0, 0);
}

void linesTexture(float alpha) {
  float step = 10;
  for (int x = 0; x < width; x += step) {
    for (int y = 0; y < height; y += step) {
      if (chance(85)) {
        float previousX = x;
        float previousY = y;
        float additionalIterations = random(5, 20);
        float scale  = random(200, 300);
        float speed  = random(.4, .9);
        float offset = random(TWO_PI);
        beginShape(LINES);
       
       for (int i = 0; i < additionalIterations; i++)
        {
          float noi = offset + noise(previousX / scale, previousY / scale) * TWO_PI;
          float newX = previousX + cos(noi) * speed;
          float newY = previousY + sin(noi) * speed;
          strokeWeight(.8);
          stroke(255, alpha);
          vertex(previousX, previousY);
          vertex(newX, newY);
          previousX = newX;
          previousY = newY;
        }
        endShape();
      }
    }
  }
}

boolean chance (int outOfAHundred) {
  return random(100) < outOfAHundred;
}

void saveTheFrame () {
  String path = sketchPath();
  File directory = new File(path + "/render");
  int count = directory.list() != null ? 0 : directory.list().length;
  String number = String.format("%04d", count);
  saveFrame("render/out_" + number + ".png");
}

void keyPressed () {
  if (key == 'g') {
    generate();
  }
  else if (key == 's') {
    saveTheFrame();
  }
}
