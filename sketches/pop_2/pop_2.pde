color[][] palettes = new color[][]{
  new color[]{#f8ffe5, #06d6a0, #1b9aaa, #ef476f, #ffc43d},
  new color[]{#7400b8, #5e60ce, #4ea8de, #56cfe1, #72efdd},
};

int paletteIndex = 1;
color[] palette = palettes[paletteIndex];

int alpha = 128;

int innerIterations = 800;
int outerIterations = 1000;

int pointSizeMin = 0;
int pointSizeMax = 5;

boolean dynamic = true;

PVector position;

void setup () {
  background(palette[1]);
  size(800,800);
  position = new PVector(0,height/2);
  
  for (int i = 0; i < outerIterations; i++) {
    redraw();
  }
}

void draw () {
  pushStyle();
  for (int i = 0; i < innerIterations; i++) {
    float noise = noise(position.x, position.y);
    
    float y = sin(i / (float)innerIterations * 60) * height / 80;
    float dynamicFactor = dynamic ? (mouseX / 10) : 1;
    
    position.y += y;
    position.x += noise;
    
    position = new PVector(position.x % width, position.y % height);
    
    stroke(palette[frameCount % palette.length], alpha / dynamicFactor);
    strokeWeight(map(noise, 0, 1, pointSizeMin, pointSizeMax) * dynamicFactor);
    point(position.x, position.y);
  }
  popStyle();
  
  pushStyle();
  
  int ellipseIterations = 360;
  float size = sin(position.x) * height / 2;
  
  noFill();
  // ellipse(position.x, position.y, size, size);
  
  for (int i = 0; i < ellipseIterations; i++) {
    
    float cos = cos(map(i, 0, ellipseIterations, 0, TWO_PI));
    float sin = sin(map(i, 0, ellipseIterations, 0, TWO_PI));
    
    float x = position.x + cos * size;
    float y = position.y + sin * size;
    
    // strokeWeight(2);
    
    stroke(palette[frameCount % palette.length], alpha);
    line(position.x, position.y, x, y);
    
  }
  popStyle();
}

void keyPressed() {
  if (!dynamic) return;
  
  if (key == CODED) {
    palette = palettes[paletteIndex++ % palettes.length];
  }
  else {
    pushStyle();
    fill(palette[frameCount % palette.length]);
    noStroke();
    rect(0, 0, width, height);
    popStyle();
  }
}
