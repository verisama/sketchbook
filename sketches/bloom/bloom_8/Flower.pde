class Flower {
  float definition = 60.0;
  float seed = 1.0;
  float weight = 1.0;
  
  int nPetals;
  
  PVector origin;
  PVector[] vertices;
  
  float size;
  float cycleLength;
  
  float paletteMin, paletteMax;
  
  public Flower (int nPetals, PVector origin, float size, float cycleLength, float paletteMin, float paletteMax, float seed) {
    this.nPetals = nPetals;
    this.origin = origin;
    this.size = size;
    this.cycleLength = cycleLength;
    
    this.paletteMin = paletteMin;
    this.paletteMax = paletteMax;
    
    this.seed = seed;
    
    this.vertices = new PVector[]{
      new PVector(0, 0),
      new PVector(0, -size / 20),
      new PVector(-size / 10, -size / 10),
      new PVector(-size / 5, -size / 5),
      new PVector(-size / 3, -size / 2),
      new PVector(0, -size),
      new PVector(size / 3,  -size / 2),
      new PVector(size / 5,  -size / 5),
      new PVector(size / 10, -size / 10),
      new PVector(0, -size / 20),
      new PVector(0, 0)
    };
    
    PVector[] moreVertices = new PVector[vertices.length * 2 - 1];
    
    int prev = vertices.length - 1;
    int next = 0;
    for (int i = 0; i < moreVertices.length; i++) {
      if (i % 2 == 0) {
        moreVertices[i] = vertices[i / 2];        
      }
      else {
        PVector v = vertices[prev++].copy().lerp(vertices[next++], random(1.0));
        // v.add(randomGaussian(), randomGaussian());
        moreVertices[i] = v;
        
        if (prev == vertices.length) prev = 0;
      }
    }
    
    vertices = moreVertices;
  }
  
  void draw () {
    pushMatrix();
    translate(origin.x, origin.y);
    
    for (int i = 0; i < nPetals; i++) {
      rotate(TWO_PI / nPetals);
    
      for (float f = 0; f < 1; f += 1.0 / definition) {
        bloom(f);
      }
    }
    popMatrix();
  }
  
  void bloom (float t) {
    pushMatrix();
    translate(origin.x, origin.y);
    
    pushStyle();
    noFill();
    color c = palette.getColor(map(t, 0, 1, paletteMin, paletteMax));
    float alpha = map(abs(0.3 - t), 0, 0.5, 128, 0);
    stroke(c, alpha);
    strokeWeight(weight);
    
    for (int i = 0; i < nPetals; i++) {
      rotate(TWO_PI / nPetals);
      petal(t);
    }
    
    popStyle();
    popMatrix();
  }
  
  void petal (float t) {
    beginShape();
    for (PVector v : vertices) {
      float r0 = noise(v.y, v.x, seed);
      float rx = noise(v.x, v.y, r0 * t);
      float ry = noise(v.x, v.y, r0 * (1-t));
      curveVertex(v.x * t * rx, v.y * t * ry);
    }
    endShape(CLOSE);
  }
  
  void bloomByFPS (float fps) {
    float t = (fps % cycleLength) / cycleLength;
    bloom(t);
  }
  
  void setSize (float size) { this.size = size; }
  float getSize () { return this.size; }
  
  void setSeed (float seed) { this.seed = seed; }
}
