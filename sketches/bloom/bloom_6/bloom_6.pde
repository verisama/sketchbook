
Flower[] flowers;

boolean savingEnabled = false;
float r = 1.0;

float cycle = 180.0;
float rotation = 0.0;
float rotationStep = radians(30);

void setup () {
  size(600, 600);
  init();
  noStroke();
  smooth();

  flowers = new Flower[] {
    new Flower(9, new PVector(0, 0), height * 0.9, 75.0, .0, 1., r), 
    // new Flower(9, new PVector(0, 0), height * 0.66, 150.0, .0, 1., r), 
    new Flower(9, new PVector(0, 0), height * 0.3, 25.0, .0, 1., r), 
  };
  palette.drawBackground();
  g();
}

void draw () {
  g();
}

void g () {  

  r = random(99999999);
  // palette.drawBackground();

  float t = (frameCount % cycle) / cycle;
  if (frameCount % cycle == 0) rotation += rotationStep;

  pushMatrix();
  translate(width / 2, height / 2);
  rotate(rotation);

  for (Flower f : flowers) {
    // filter(BLUR);
    f.setSize(f.getSize() + noise(r) * randomGaussian() * 10);
    f.setSeed(r);
    // f.draw();
    f.bloom(t);
  }
  popMatrix();

  // texturizeAll();

  if (savingEnabled) s();
}

void s () {
  saveTheFrame();
  println("Saved!");
  // noLoop();
}
