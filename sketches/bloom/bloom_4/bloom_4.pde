Flower f1, f2, f3;

boolean saved = false;

float cycle = 75.0;
float rotation = 0.0;
float rotationStep = radians(60);

void setup () {
  size(600, 600);
  init();
  palette.drawBackground();
  noStroke();
  smooth();
  f1 = new Flower(height * 0.9);
  f2 = new Flower(height * 0.66);
  f3 = new Flower(height * 0.3);
}

void draw () {
  translate(width / 2, height / 2);
  rotate(rotation);
  if (frameCount % cycle == 0) rotation += rotationStep;
  
  f1.bloom((frameCount % cycle) / cycle);
  f2.bloom((frameCount % cycle) / cycle);
  f3.bloom((frameCount % cycle) / cycle);
  
  if (!saved && rotation > TWO_PI - rotationStep) {
    saveTheFrame();
    saved = true;
    // stop();
  }
}
