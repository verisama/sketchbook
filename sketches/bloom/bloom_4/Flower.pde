class Flower {
  float size;
  PVector[] vertices;
  
  public Flower (float size) {
    this.size = size;
    this.vertices = new PVector[]{
      new PVector(0, 0),
      new PVector(0, 0),
      new PVector(-size / 10, -size / 10),
      new PVector(-size / 5, -size / 5),
      new PVector(-size / 3, -size / 2),
      new PVector(0, -size),
      new PVector(size / 3,  -size / 2),
      new PVector(size / 5,  -size / 5),
      new PVector(size / 10, -size / 10),
      new PVector(0, 0),
      new PVector(0, 0)
    };
    
  }
  
  void bloom (float t) {
    pushStyle();
    noFill();
    stroke(palette.getColor(t), map(abs(0.3 - t), 0, 0.5, 255, 0));
    // stroke(palette.getColor(t), 255 - t * 255);
    
    
    
    beginShape();
    for (PVector v : vertices) {
      float r0 = noise(v.y, v.x);
      float rx = noise(v.x, v.y, r0 * t);
      float ry = noise(v.x, v.y, r0 * (1-t));
      curveVertex(v.x * t * rx, v.y * t * ry);
    }
    endShape(CLOSE);
 
    popStyle();
  }
  
  void setSize (float size) {
    this.size = size;
  }
}
