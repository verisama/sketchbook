class Flower {
  float size;
  PVector[] vertices;
  float paletteMin, paletteMax;
  
  public Flower (float size, float paletteMin, float paletteMax) {
    this.size = size;
    this.vertices = new PVector[]{
      new PVector(0, 0),
      new PVector(0, 0),
      new PVector(-size / 10, -size / 10),
      new PVector(-size / 5, -size / 5),
      new PVector(-size / 3, -size / 2),
      new PVector(0, -size),
      new PVector(size / 3,  -size / 2),
      new PVector(size / 5,  -size / 5),
      new PVector(size / 10, -size / 10),
      new PVector(0, 0),
      new PVector(0, 0)
    };
    
    this.paletteMin = paletteMin;
    this.paletteMax = paletteMax;
  }
  
  void bloom (float t) {
    pushStyle();
    noFill();
    stroke(palette.getColor(map(t, 0, 1, paletteMin, paletteMax)), map(abs(0.3 - t), 0, 0.5, 255, 0));
    // stroke(palette.getColor(t), 255 - t * 255);
    
    
    
    beginShape();
    for (PVector v : vertices) {
      float r0 = noise(v.y, v.x);
      float rx = noise(v.x, v.y, r0 * t);
      float ry = noise(v.x, v.y, r0 * (1-t));
      curveVertex(v.x * t * rx, v.y * t * ry);
    }
    endShape(CLOSE);
 
    popStyle();
  }
  
  void setSize (float size) {
    this.size = size;
  }
}
