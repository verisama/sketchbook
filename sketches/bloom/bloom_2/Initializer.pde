import dawesometoolkit.*;
DawesomeToolkit dawesome;

Palette palette;

void init () {
  palette = new Palette("63372c-c97d60-F9DBBD-f2e5d7-DB5461", PaletteLerpMode.LINEAR); // generated with https://coolors.co/app
  palette.setBackground(#262322);
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}
