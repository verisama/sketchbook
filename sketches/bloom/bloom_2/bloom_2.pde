Flower f;

float cycle = 180.0;
float rotation = 0.0;
float rotationStep = radians(90);

void setup () {
  size(600, 600);
  init();
  palette.drawBackground();
  noStroke();
  smooth();
  f = new Flower(height / 3 * 2);
}

void draw () {
  
  translate(width / 2, height / 2);
  rotate(rotation);
  if (frameCount % cycle == 0) rotation += rotationStep;
  
  f.bloom((frameCount % cycle) / cycle);
  
  // f.bloom(0.5);
}

class Flower {
  float size;
  PVector[] vertices;
  
  public Flower (float size) {
    this.size = size;
    this.vertices = new PVector[]{
      new PVector(0, 0),
      new PVector(0, 0),
      new PVector(-size / 10, -size / 10),
      new PVector(-size / 5, -size / 5),
      new PVector(-size / 3, -size / 2),
      new PVector(0, -size),
      new PVector(size / 3,  -size / 2),
      new PVector(size / 5,  -size / 5),
      new PVector(size / 10, -size / 10),
      new PVector(0, 0),
      new PVector(0, 0)
    };
  }
  
  void bloom (float t) {
    pushStyle();
    // noStroke();
    noFill();
    stroke(palette.getColor(t), 255 - t * 255);
    //fill(palette.getColor(t));
    
    // ellipse(0, 0, size * t, size * t);
    
    beginShape();
    for (PVector v : vertices) {
      float rx = noise(v.x, v.y, t);
      float ry = noise(v.x, v.y, 1-t);
      curveVertex(v.x * t * rx, v.y * t * ry);
    }
    endShape(CLOSE);
    popStyle();
  }
}
