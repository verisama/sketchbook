// FILLING ME SOFTLY

PVector origin = new PVector(
  width / 2 + random(width / 4),
  height / 2 + random(height / 4)
);

int times = 2;
float size = 100;
float speed = 10;
color bg = #651386;

float noiseZoom = 0.01;
float noiseZoomMovement = 0.1;

int[] polygons = new int[]{ 3, 5, 7 };

void setup (){
  background(bg);
  // blendMode(DIFFERENCE);
  size(800, 800);
  
  for (int i = 0; i < 200; i++) paint();
}

void draw () {
  if (frameCount % 6 == 0) bg(bg, 1);
  
  paint();
  
  if (frameCount % 12 == 0) paintPolygon();
}


void paintPolygon () {
  float sides = polygons[int(random(polygons.length))];
  float radius = abs(size * randomGaussian() * 1.5);
  
  pushMatrix();
  pushStyle();
  translate(width / 2, height / 2);
  rotate(random(TWO_PI));
  beginShape();
  noFill();
  stroke(0);
  strokeWeight(pow(radius / 81, 3)); 

  for (int i = 0; i < sides; i++) {
    float t = TWO_PI / sides * i;
    float x = cos(t) * radius;
    float y = sin(t) * radius;
    vertex(x, y);
  }
  endShape(CLOSE);
  popStyle();
  popMatrix();
}

void paint() {

  for (int i = 0; i < times; i++) {
    noiseBrush(origin.x, origin.y, size, noiseZoom);
  }
  
  float y = noise(
    (origin.x + randomGaussian()) * noiseZoomMovement,
    origin.y + randomGaussian() * noiseZoomMovement
  ) * speed;
  
  origin = origin.add(new PVector(speed,  y * speed));
  
  if (origin.x > width || origin.x < 0) speed *= -1;
  if (origin.y > height || origin.y < 0) origin.y = abs(origin.y) % height;
}


void noiseBrush(float x, float y, float size, float zoom) {
  for (int i=0; i<size; i++) {
    for (int j=0; j<size; j++) {
      float dist = dist(j, i, size/2, size/2);

      if (dist > size) return;

      float posX = (j - size/2) + x;
      float posY = (i - size/2) + y;

      float noiseX = posX * zoom, noiseY = posY * zoom;
      float n = noise(noiseX, noiseY);

      float alphaJitter = randomGaussian() * n * 30;
      float alpha = map(dist, 0, size/2, 255, 0) + alphaJitter;

      color c = color(blue(bg) * n, 0, red(bg) * n);
      stroke(c, alpha);
      point(posX, posY);
    }

  }
}


void bg(color c, int opacity) {
  pushMatrix();
  pushStyle();
  noStroke();
  fill(c, opacity);
  rect(0, 0, width, height);
  popStyle();
  popMatrix();
}
