Blob b;

void setup() {
  background(0);
  size(800, 600, P2D);
  noStroke();
  smooth(10);
  
  init();
  
  colorMode(HSB, 360, 100, 100, 100);
  
  b = new Blob(300);
}

void draw() {
  // background(0);
  float a = radians(frameCount % 360);
  // translate(width >> 1, height >> 1);
  // b.paint(cos(a) * width / 3, sin(a) * height / 3);
  b.paint(mouseX, mouseY);
  b.mutate(frameCount);
}
