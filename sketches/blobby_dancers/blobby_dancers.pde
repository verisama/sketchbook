Blob b;
BlobbyDancer[] dancers;

int radius = 200;

int N_DANCERS = 8;
float MIN_SPEED =.5, MAX_SPEED = 2;

void setup() {
  background(0);
  // size(800, 600, P2D);
  size(800, 600);
  noStroke();
  smooth(10);
  
  blendMode(DIFFERENCE);
  
  init();
  
  // colorMode(HSB, 360, 100, 100, 100);
  
  b = new Blob(radius);
  dancers = new BlobbyDancer[N_DANCERS];
  
  float speed = random(MIN_SPEED, MAX_SPEED);
  PVector center = new PVector(width / 2, height / 2);
  
  for (int i = 0; i < N_DANCERS; i++) {
    PVector direction = PVector.fromAngle(float(i) / N_DANCERS * TWO_PI);
    dancers[i] = new BlobbyDancer(center, direction, random(radius), speed + randomGaussian() / 4);
  }
}

void draw() {
  // background(0);
  palette.drawBackground(2);
  float a = radians(frameCount % 360);
  // translate(width >> 1, height >> 1);
  // b.paint(cos(a) * width / 3, sin(a) * height / 3);
  // b.paint(mouseX, mouseY);
  // b.mutate(frameCount);
  
  for (Dancer dancer : dancers) {
    dancer.update();
    dancer.draw(2, #00ff00);
  }
}
