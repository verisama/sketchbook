class BlobbyDancer extends Dancer {
  PVector[] vertices;
  
  float alpha = 10;
  float size;
  float noiseDetail = 0.2;
  
  float directionNoiseFactor = 0.1;
  
  Blob blobby;
  
  public BlobbyDancer(PVector origin, PVector direction, float size, float speed) {
    super(origin, direction, size, speed);
    
    this.size = size;
    
    blobby = new Blob(size);
    transform.rotation = radians(random(360));
    
    if (!inBounds()) { transform.position = new PVector(width >> 1 , height >> 1); }
  }
  
  boolean inBounds() {
    return transform.inBounds(
      0, 0,
      width, height
     );
  }
  
  
  
  void update() {
    super.update();
    updateDirection();
    updatePosition();
    updateRotation();
    updateScale();
  }
  
  void updateDirection() {
    direction.x += noise(transform.position.y * noiseDetail) * randomGaussian() * directionNoiseFactor;
    direction.y += noise(transform.position.x * noiseDetail) * randomGaussian() * directionNoiseFactor;
  }
  
  void updatePosition() {
    transform.move(direction, speed);
    
    if (!inBounds()) {
      circle(transform.position.x, transform.position.y, size * 1.2);
      invertDirection();
      transform.move(direction, speed * 2);
    }
  }
  
  void updateRotation() {
    transform.turnDegrees(3 * noise(transform.position.x, transform.position.y));
  }
  
  void updateScale() {
    blobby.mutate(frameCount);
  }
  
  void setSpeed(float v) { speed = v; }
  
  void setAlpha(float v) { alpha = v; }
  
  void invertDirection() {
    direction.x = -direction.x;
    direction.y = -direction.y;
  }
  
  void draw1(color c) {
    blobby.paint(0,0, "fill");
  }
  
  void draw2(color c) {
    blobby.paint(0,0,"stroke");
  }
}
