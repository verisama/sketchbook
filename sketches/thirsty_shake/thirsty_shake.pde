int iterations = 3;
float x = 10, y = 10, size = 5;

color bg = #9d3dd3;
color fg = #3dd39d;

void settings () {
  size(400, 400);
}

void setup() {
  colorMode(HSB, 360, 100, 100);
  background(bg);
  fill(fg);
  stroke(fg);
  strokeWeight(2);
}


void curiousLine(float x1, float y1, float x2, float y2) {
  float distance =  dist(x1, y1, x2, y2);
  int segments = (int)(distance / noise(x1, y1) / 2);
  
  float x = x2 - x1, y = y2 - y1;

  pushMatrix();
  beginShape();
  vertex(x1, y1);

  PVector direction = new PVector(x2, y2).sub(new PVector(x1, y1));
  
  for (int i = 0; i < segments; i++) {
    float nextX = x / segments * i + noise(x1, y1) * distance;
    float nextY = y / segments * i + noise(x1, y1) * distance / 2;
    
    translate(x1, y1);
    line(0, 0, nextX, nextY);
    // ellipse(nextX, nextY, 3, 3);
    
    x1 = nextX;
    y1 = nextY;
    
  }
  endShape();
  popMatrix();

}

void draw() {
  float angle = TWO_PI / iterations;

  translate(width/2, height/2);
  for (int i = 0; i < iterations; i++) {
    rotate(angle);
    ellipse(x, y, size, size);
    curiousLine(cos(angle) * x * size, sin(angle) * y * size, x, y);
  }
}

void keyPressed() {
  if (key == CODED) {
    switch(keyCode) {
      case UP:
        iterations++;
        break;
      
      case DOWN:
        if (iterations == 3) return;
        iterations--;
        break;
    }
  }
}
