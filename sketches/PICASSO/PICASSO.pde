Surface bg1, bg2;
Lips lips;

void setup () {
  size(600, 800);
  noStroke();

  drawFace();
}

void drawFace() {
  bg1 = new Surface(
    new PVector[]{
    new PVector(0, 0),
    new PVector(width * 0.8, 0),
    new PVector(width * 0.6, height),
    new PVector(0, height)
    },
    #551144
    );
  bg2 = new Surface(
    new PVector[]{
    new PVector(width * 0.8, 0),
    new PVector(width, 0),
    new PVector(width, height),
    new PVector(width * 0.6, height),
    },
    #160516
    );
    
  lips = new Lips();
  bg1.paint();
  bg2.paint();
  
  translate(width/2, height/2);
  lips.paint();
}

class Surface {
  PVector[] vertices;
  color c;

  public Surface () {
  }
  
  public Surface (PVector[] vertices, color c) {
    this.vertices = vertices;
    this.c = c;
  }

  void changeColor(color c) {
    this.c = c;
  }

  void paint () {
    noStroke();
    fill(c);

    beginShape();
    for (PVector v : vertices) {
      pVertex(v);
    }
    pVertex(vertices[0]);
    endShape(CLOSE);
  }
}

class Lips extends Surface {
  float size = 80;
    
  public Lips () {
    this.vertices = (new PVector[]{
     new PVector(-size/2, 0),
     new PVector(0, -size / 4),
     new PVector(size / 4, 0),
     new PVector(size / 3, -size/4),
     new PVector(size/2, 0),
     new PVector(size/3, 0),
     new PVector(size/2, size/4),
     new PVector(size/4, size/2)
    });
    this.c = #000033;
  }
}


void pVertex (PVector v) {
  vertex(v.x, v.y);
}
