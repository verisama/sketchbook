import java.util.Date;

float size = 100;
color bg = #651386;

float noiseZoom = 0.01;
float noiseZoomMovement = 0.1;

int[] polygons = new int[]{ 3, 5, 7 };

boolean SAVE_FRAMES = true;
int SAVE_FRAME_RATE = 6;

void setup (){
  background(bg);
  blendMode(DIFFERENCE);
  size(800, 800);
  
  for (int i = 0; i < 200; i++) paintPolygon();
}

void draw () {
  if (frameCount % 6 == 0) bg(bg, 1);
  if (frameCount % 12 == 0) paintPolygon();
  
  if (SAVE_FRAMES && frameCount % SAVE_FRAME_RATE == 0) saveFrame("diffidum_" + new Date().getTime() + "_####.tga");
}


void paintPolygon () {
  float clock = frameCount % frameRate;

  float sides = polygons[int(random(polygons.length))];

  // float radius = abs(size * randomGaussian() * (frameCount % 60) / 15);
  float radius = size * clock / random(10, 30);
  
  pushMatrix();
  pushStyle();
  translate(width / 2, height / 2);
  rotate(TWO_PI / (frameRate * 3) * clock);
  beginShape();
  noFill();
  stroke(128);
  
  float sw = pow(radius / 81, 3);
  strokeWeight(sw < 2 ? 2 : sw);

  for (int i = 0; i < sides; i++) {
    float t = TWO_PI / sides * i;
    float x = cos(t) * radius;
    float y = sin(t) * radius;
    vertex(x, y);
  }
  endShape(CLOSE);
  popStyle();
  popMatrix();
}

void bg(color c, int opacity) {
  pushMatrix();
  pushStyle();
  noStroke();
  fill(c, opacity);
  rect(0, 0, width, height);
  popStyle();
  popMatrix();
}
