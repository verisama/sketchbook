class Orchestrator {
  PApplet context;
  Media media;
  
  float peakLevel = 0.65;
  float midLevel = 0.275;
  float lowLevel = 0.1;
  
  float lastBeat = 0;
  float lastPeak = 0;
  float lastMid = 0;
  float lastLow = 0;
  
  public Orchestrator (PApplet context, Media media) {
    this.context = context;
    this.media = media;
  }
  
  public void setup () {
  }
  
  public void play () {
    media.play();
  }
  
  public void orchestrate () {
    if (!media.isPlaying()) return;
    
    if (media.isBeating()) onBeat();
    
    if (media.level() >= peakLevel) onLevelPeak();
    if (media.level() >= midLevel) onLevelMid();
    if (media.level() >= lowLevel) onLevelLow();
    
    always();
  }
  
  public void onBeat () {
    lastBeat = media.position();
  }
  
  public void onLevelPeak () {
    lastPeak = media.position();
  }
  
  public void onLevelMid () {
    lastMid = media.position();
  }
  
  public void onLevelLow () {
    lastLow = media.position();
  }
  
  public float sinceLastBeat() { return media.position() - lastBeat; }
  
  public float sinceLastPeak() { return media.position() - lastPeak; }
  
  public float sinceLastMid() { return media.position() - lastMid; }
  
  public float sinceLastLow() { return media.position() - lastLow; }
  
  public void always () {
  }
}
