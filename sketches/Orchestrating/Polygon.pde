class Polygon {
    
    int sides;
    int radius;
    float angle;


    Polygon(float sides, float radius){
        this.sides = int(sides);
        this.radius = int(radius);
        this.angle = TWO_PI/sides;
    }
    Polygon(int sides, int radius){
        this.sides = sides;
        this.radius = radius;
        this.angle = TWO_PI/sides;
    }

    void draw(int oX, int oY){
        beginShape();
        for(int i=0; i<sides; i++){
            vertex(oX+cos(i*angle)*radius, oY+sin(i*angle)*radius);
        }
        endShape(CLOSE);
    }
}
