String versionCode = "" + ("" + random(1000)).hashCode();

void mousePressed() {
    println(song.position());
    if (song.isPlaying()) {
        song.pause();
    } else {
        song.play();
    }
}

void keyPressed() {
    if (key == 's' || key == 'S') {
        String name = new File(sketchPath()).getName();
        String filename = String.join("_", new String[]{name, versionCode, "####.png"});
        saveFrame(filename);
    }
    else if(key == 'f' || key == 'F') {
        song.jump(song.position() + 1);
    }
}
