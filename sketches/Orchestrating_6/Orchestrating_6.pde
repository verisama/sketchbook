import processing.sound.*;

public static final float SCALE = 80;
public static final int W = (int)SCALE * 16;
public static final int H = (int)SCALE * 9;

public static final int PADDING = W/20; //(px)


color c;
int i, x1, y1, x2, y2;
int iterations;
float theta = 2f*PI/iterations;

BlobbyOrchestrator orchestrator;
Media media;
SoundFile song;

boolean DEBUG = false;

void settings () {
  size(W, H);
  smooth();
}

void setup() {
  init();

  blendMode(DIFFERENCE);
  
  media = new Media(this, "mp3");
  song = media.loadAudio(media.getFile(2));
  song.jump(50); // seconds
  song.pause();
  orchestrator = new BlobbyOrchestrator(this, media);

  background(0);

  orchestrator.setup();
  // orchestrator.play();
}

void draw() {
  orchestrator.orchestrate();
}
