class BlobbyOrchestrator extends Orchestrator {

  Blob blob;
  int intensityThreshold = 10;
  int bandScale = 100;

  ArrayList<BlobbyDancer> dancers;
  
  PVector center;
  
  public BlobbyOrchestrator (PApplet context, Media media) {
    super(context, media);

    this.dynamicLevel = true;
    
    blob = new Blob(height / 2);
    
    dancers = new ArrayList<BlobbyDancer>();
    
    center = center();
  }

  public void setup () {
    colorMode(HSB, 360, 100, 100, 100);
  }
  
  public PVector center () {
    return new PVector(width / 2, height / 2);
  }
  public PVector up () { return new PVector(0, -1); }
  public PVector down () { return new PVector(0, 1); }
  public PVector left () { return new PVector(-1, 0); }
  public PVector right () { return new PVector(1, 0); }
  
  public void always() {
    super.always();

    // background(0);
    palette.drawBackground(128);

    blob.mutate(frameCount, sinceLastPeak());
  }

  public void onBeat () {
    super.onBeat();

    background(128);
  }

  public void onLevelPeak () {
    super.onLevelPeak();
  }

  public void onLevelMid () {
    super.onLevelMid();

    fill(180, 80, 80);
  }

  public void onLevelLow () {
    super.onLevelLow();

    fill(90, 80, 80);
  }
  
  public void addDancer(PVector direction, float size, float speed, float lifetime) {
    BlobbyDancer dancer = new BlobbyDancer(center(), direction, size, speed, lifetime);
    dancers.add(dancer);
  }
  
  public void addDancerUp(float size, float speed, float lifetime) {
    addDancer(up(), size, speed, lifetime);
  }
  public void addDancerDown(float size, float speed, float lifetime) {
    addDancer(down(), size, speed, lifetime);
  }
  public void addDancerLeft(float size, float speed, float lifetime) {
    addDancer(left(), size, speed, lifetime);
  }
  public void addDancerRight(float size, float speed, float lifetime) {
    addDancer(right(), size, speed, lifetime);
  }

  public void onBand (int band, float frequency, float power) {
    float intensity = power * bandScale; // * pow(log(band) * 3, 2);

    if (frequency > L_L_THRESHOLD && frequency <= L_U_THRESHOLD) {
      onLowFrequencies(band, frequency, intensity);
    } else if (frequency > M_L_THRESHOLD && frequency <= M_U_THRESHOLD) {
      onMidFrequencies(band, frequency, intensity);
    } else if (frequency > H_L_THRESHOLD && frequency <= H_U_THRESHOLD) {
      onHighFrequencies(band, frequency, intensity);
    }

    
    if (band < media.frequencyToBand(200)) {
      if (intensity > 30) onKick(intensity);
      else onKickOff(intensity);
    }
    else if (band < media.frequencyToBand(2000)) {
      if (intensity > 30) onMid(intensity);
    }
    
    if (band < media.frequencyToBand(8000)) {
      if (intensity > 10) onHigh(intensity);
    }
    
    if (DEBUG) {
      float hue = map(band, 0, media.FFT_BANDS, 0, 360);
      fill(hue, 80, 80);
      circle(map(band, 0, media.FFT_BANDS, 0, width), height/2, intensity);
    }
  }
  
  boolean bandBetween(int value, int lower, int upper) {
    return value >= lower && value <= upper;
  }
  
  public void onKick(float intensity) {
    fill(180, 100, 100);
    blob.paint(center(), intensity * 50, "fill", intensity / 200);
  }
  
  public void onKickOff(float _intensity) {
    blob.reset();
  }
  
  public void onMid(float _intensity) {
    background(0, 100, 100);
  }
  
  public void onHigh(float intensity) {
    fill(60, 100, 100);
    circle(center.x, center.y, intensity / 100 * height);
  }

  public void onLowFrequencies(int band, float frequency, float intensity) {
  }

  public void onMidFrequencies(int band, float frequency, float intensity) {
  }

  public void onHighFrequencies(int band, float frequency, float intensity) {
  }

}
