color[][] palettes = new color[][]{
  new color[]{#f8ffe5, #06d6a0, #1b9aaa, #ef476f, #ffc43d},
  new color[]{#7400b8, #5e60ce, #4ea8de, #56cfe1, #72efdd},
};

int paletteIndex = 1;
color[] palette = palettes[paletteIndex];


int innerIterations = 800;
int outerIterations = 1000;

int pointSizeMin = 0;
int pointSizeMax = 5;

boolean dynamic = true;

PVector position;

void setup () {
  background(palette[1]);
  size(800,800);
  position = new PVector(0,height/2);
  
  for (int i = 0; i < outerIterations; i++) {
    redraw();
  }
}

void draw () {
  for (int i = 0; i < innerIterations; i++) {
    float noise = noise(position.x, position.y);
    
    float y = sin(i / (float)innerIterations * 60) * height / 80;
    float dynamicFactor = dynamic ? (mouseX / 10) : 1;
    
    position.y += y;
    position.x += noise;
    
    position = new PVector(position.x % width, position.y % height);
    
    stroke(palette[frameCount % palette.length]);
    strokeWeight(map(noise, 0, 1, pointSizeMin, pointSizeMax) * dynamicFactor);
    point(position.x, position.y);
  }
}

void keyPressed() {
  if (!dynamic) return;
  
  palette = palettes[paletteIndex++ % palettes.length];
}
