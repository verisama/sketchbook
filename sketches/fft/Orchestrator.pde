class Orchestrator {
  PApplet context;
  Media media;

  float peakLevel = 0.65;
  float midLevel = 0.275;
  float lowLevel = 0.1;

  float lastBeat = 0;
  float lastPeak = 0;
  float lastMid = 0;
  float lastLow = 0;

  float lastKick = 0;
  float lastSnare = 0;
  float lastHat = 0;

  boolean dynamicLevel = true;

  public Orchestrator (PApplet context, Media media) {
    this.context = context;
    this.media = media;

    if (dynamicLevel) {
      peakLevel = 0;
      midLevel = 0;
      lowLevel = 0;
    }
  }

  public void setup () {
  }

  public void play () {
    media.play();
  }

  public void orchestrate () {
    if (!media.isPlaying()) return;

    if (dynamicLevel) {
      peakLevel = max(media.level(), peakLevel);
      midLevel = peakLevel / 2;
      lowLevel = midLevel / 2;
    }

    if (media.isBeating()) onBeat();

    if (media.level() >= peakLevel) onLevelPeak();
    else if (media.level() >= midLevel) onLevelMid();
    else if (media.level() >= lowLevel) onLevelLow();
    
    if (media.beat.isKick()) { background(#ff0000); onKick(); }
    if (media.beat.isSnare()) onSnare();
    if (media.beat.isHat()) onHat();

    always();
  }

  public void onBeat () {
    lastBeat = media.position();
  }

  public void onLevelPeak () {
    lastPeak = media.position();
  }

  public void onLevelMid () {
    lastMid = media.position();
  }

  public void onLevelLow () {
    lastLow = media.position();
  }
  

  public void onKick () {
    lastKick = media.position();
  }

  public void onSnare () {
    lastSnare = media.position();
  }

  public void onHat () {
    lastHat = media.position();
  }


  public void always () {
  }
  

  public float sinceLastBeat() {
    return media.position() - lastBeat;
  }

  public float sinceLastPeak() {
    return media.position() - lastPeak;
  }

  public float sinceLastMid() {
    return media.position() - lastMid;
  }

  public float sinceLastLow() {
    return media.position() - lastLow;
  }



  public float sinceLastKick() {
    return media.position() - lastKick;
  }

  public float sinceLastSnare() {
    return media.position() - lastSnare;
  }

  public float sinceLastHat() {
    return media.position() - lastHat;
  }
  
  
}
