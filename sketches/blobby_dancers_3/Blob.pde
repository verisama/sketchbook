class Blob {
  float radius, minRadius;
  
  int nPoints = 200;
  float angle = TWO_PI / nPoints;
  float noiseFactor = 0.002f;
  float timeFactor = 0.01f;
  PVector[] vertices;

  public Blob(float radius) {
    this.radius = radius;
    this.minRadius = radius * 0.2;
    this.vertices = new PVector[nPoints];
    
    for (int i = 0; i < nPoints; i++) {
      vertices[i] = calculateVertex(i);
    }
  }
  
  PVector calculateVertex(int i) {
    return new PVector(
        cos(angle * i) * radius,
        sin(angle * i) * radius
       ).normalize();
  }
  
  public void mutate(float t, float noisiness) {
    float n;


    for (PVector v : vertices) {
    float x = v.x * noiseFactor * noisiness + t * timeFactor;
    float y = v.y * noiseFactor * noisiness + t * timeFactor;
      n = noise(x, y);
      v.setMag(map(n, 0, 1, minRadius, radius));
    }
  }
  
  public color getColor(PVector  v) {
    return palette.getColor(v.mag());
  }

  public void paint(float x, float y, float noisiness, String mode) {
    pushMatrix();
    translate(x, y);

    beginShape();
    
    for (PVector v : vertices) {
      if (mode == "fill") fill(getColor(v));
      else if (mode == "stroke") {strokeWeight(radius / 5); stroke(getColor(v)); }
      
      curveVertex(v.x, v.y);
    }

    endShape(CLOSE);
    popMatrix();
  }
  
  public void paint(PVector o, float noisiness, String mode) {
    paint(o.x, o.y, noisiness, mode);
  }
}
