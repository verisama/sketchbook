Blob b;
ArrayList<Dancer> dancers;

//int radius = 200;
int radius = 100;

int N_DANCERS = 8;
float MIN_SPEED =.5, MAX_SPEED = 2;

PVector center;

void setup() {
  background(0);
  // size(800, 600, P2D);
  size(800, 600);
  noStroke();
  smooth(10);
  
  blendMode(DIFFERENCE);
  
  init();
  
  // colorMode(HSB, 360, 100, 100, 100);
  
  b = new Blob(radius);
  dancers = new ArrayList<Dancer>(N_DANCERS);
  
  float speed = random(MIN_SPEED, MAX_SPEED);
  center = new PVector(width / 2, height / 2);
  
  for (int i = 0; i < N_DANCERS; i++) {
    PVector direction = PVector.fromAngle(float(i) / N_DANCERS * TWO_PI);
    dancers.add(new BlobbyDancer(center, direction, random(radius), speed + randomGaussian() / 4));
  }
}

void draw() {
  // background(0);
  palette.drawBackground(mouseY100());
  float a = radians(frameCount % 360);
  // translate(width >> 1, height >> 1);
  // b.paint(cos(a) * width / 3, sin(a) * height / 3);
  // b.paint(mouseX, mouseY);
  // b.mutate(frameCount);
  
  for (Dancer dancer : dancers) {
    dancer.setNoisiness(mouseX100() / 2);
    dancer.update();
    dancer.draw(2, #00ff00);
  }
}

void addDancerAtCenter() {
  addDancerAt(center);
}

void addDancerAt(PVector position) {
  float lifespan = 10 * frameRate;
  float speed = random(MIN_SPEED, MAX_SPEED);
  Dancer dancer = new BlobbyDancer(position, PVector.random2D(), random(radius), speed,(int)lifespan);
  dancers.add(dancer);
}

void killDancer(float i) {
  dancers.remove(int(i));
}

void mousePressed() {
  // addDancerAtCenter();
  
  if (mouseButton == RIGHT) {
    killDancer(random(dancers.size()));
  }
  else if (mouseButton == LEFT) {
    addDancerAt(new PVector(mouseX, mouseY));
  }
}
