import dawesometoolkit.*;
DawesomeToolkit dawesome;

void init () {
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}
