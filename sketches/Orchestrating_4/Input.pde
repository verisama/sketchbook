void mousePressed() {
    println(song.position());
    if (song.isPlaying()) {
        song.pause();
    } else {
        song.play();
    }
}

void keyPressed() {
    if (key == 's' || key == 'S') {
        saveFrame("Mandala####.png");
    }
    else if(key == 'f' || key == 'F') {
        song.jump(1);
    }
}
