class ExampleOrchestrator extends Orchestrator {
 
  public ExampleOrchestrator (PApplet context, Media media) {
    super(context, media);
    
    this.dynamicLevel = true;
  }
  
  public void setup () {
    colorMode(HSB, 360, 100, 100, 100);
  }
  
  public void onBeat () {
    super.onBeat();
    
    fill(0);
    
    circle(width/2, height/2, 50);
  }
  
  public void onLevelPeak () {
    super.onLevelPeak();
    
    fill(360, 80, 80);
    circle(width/2, height/5, 50);
  }
  
  public void onLevelMid () {
    super.onLevelMid();
    
    fill(180, 80, 80);
    circle(width/2, height/5 * 2, 50);
  }
  
  public void onLevelLow () {
    super.onLevelLow();
    
    fill(90, 80, 80);
    circle(width/2, height/5 * 4, 50);
  }
  
  public void onBand (int band, float frequency, float power) {
    float intensity = power * scaleFactor * pow(log(band) * 3, 2);
    
    if (intensity < 10) return;
    
    float hue = map(band, 0, media.FFT_BANDS, 0, 360);
    fill(hue, 80, 80);
    circle(map(band, 0, media.FFT_BANDS, 0, width), height/2, intensity);
  }
  
  
  public void always () {
    super.always();
    
    background(0);
  }
}
