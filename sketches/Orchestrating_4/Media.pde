class Media {
  // Using just the path of this sketch to demonstrate,
  // but you can list any directory you like.
  String path;
  String format;
  
  ArrayList<File> files;
  
  SoundFile audio;
  FFT fft;
  BeatDetector beat;
  Amplitude amp;
  
  PApplet context;
  
  int FFT_BANDS = 256;
  
  public Media (PApplet context, String format) {
    this.context = context;
    this.path = join(new String[]{sketchPath(), "data"}, "/");
    this.format = format;
    this.files = new ArrayList<File>();
    
    this.read();
  }
  
  public SoundFile loadAudio (String name) {
    this.audio = new SoundFile(context, name);
    this.fft = new FFT(context, FFT_BANDS);
    this.beat = new BeatDetector(context);
    this.amp = new Amplitude(context);
    
    beat.sensitivity(300);
    
    fft.input(this.audio);
    amp.input(this.audio);
    
    return this.audio;
  }

  private void read () {
    File[] allFiles = listFiles(path);

    for (File f : allFiles) {
      if (!f.isDirectory() && match(f.getName(), format) != null) {
        files.add(f);
      }
    }  
  }
  
  public String getFile(int i) {
    File f = files.get(i % files.size());
    
    return f.getName();
  }
  
  public String getRandomFile() {
    int i = (int) random(files.size());
    
    return getFile(i);
  }
  
  public SoundFile getSong () {
    return audio;
  }
  
  public void play() {
    audio.play();
  }
  
  public void pause() {
    audio.pause();
  }
  
  public void stop() {
    audio.stop();
  }
  
  public float position () {
    return song.position();
  }
  
  public float length () {
    return song.duration(); // in seconds
  }
  
  public float level() {
    return this.amp.analyze();
  }
  
  public boolean isPlaying() {
    return audio.isPlaying();
  }
  
  public boolean isBeating() {
    return beat.isBeat();
  }

  /**
   * The frequency associated with each band of the spectrum is
   * frequency = binIndex * sampleRate / (2*numBands)
   */
  public float bandToFrequency(int bandIndex) {
    return bandIndex * audio.sampleRate() / 2 * FFT_BANDS;
  }
}
