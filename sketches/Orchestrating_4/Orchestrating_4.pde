import processing.sound.*;

public static final float SCALE = 80;
public static final int W = (int)SCALE * 16;
public static final int H = (int)SCALE * 9;

public static final int PADDING = W/20; //(px)

//FREQUENCY THRESHOLDS
/*
public static final int L_L_THRESHOLD = 10;
 public static final int L_U_THRESHOLD = 200;
 public static final int M_L_THRESHOLD = L_U_THRESHOLD;
 public static final int M_U_THRESHOLD = 4000;
 public static final int H_L_THRESHOLD = M_U_THRESHOLD;
 public static final int H_U_THRESHOLD = 15000;
 */

color c;
int i, x1, y1, x2, y2;
int iterations;
float theta = 2f*PI/iterations;

ExampleOrchestrator orchestrator;
Media media;
SoundFile song;

void settings () {
  size(W, H);
  smooth();
}

void setup() {
  init();

  media = new Media(this, "mp3");
  song = media.loadAudio(media.getFile(0));
  // song.jump(40); // seconds
  orchestrator = new ExampleOrchestrator(this, media);

  background(0);

  orchestrator.setup();
  orchestrator.play();
}

void draw() {
  orchestrator.orchestrate();

  // if (media.position() >= media.length() - 4000) exit();
}
