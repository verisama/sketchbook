/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/9584*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */

import ddf.minim.*;
import ddf.minim.analysis.*;


public static final float SCALE = 80;
public static final int W = (int)SCALE * 16;
public static final int H = (int)SCALE * 9;

public static final int PADDING = W/20; //(px)

//FREQUENCY THRESHOLDS
/*
public static final int L_L_THRESHOLD = 10;
 public static final int L_U_THRESHOLD = 200;
 public static final int M_L_THRESHOLD = L_U_THRESHOLD;
 public static final int M_U_THRESHOLD = 4000;
 public static final int H_L_THRESHOLD = M_U_THRESHOLD;
 public static final int H_U_THRESHOLD = 15000;
 */
BeatDetect beat;

color c;
int i, x1, y1, x2, y2;
int iterations;
float theta = 2f*PI/iterations;

String songName = "Climb.mp3";

Minim minim;
AudioPlayer song;
FFT fft;

Polygon poly;

PFont font;

void setup() {
    size(W, H);
    smooth();
    colorMode(HSB, 360, 100, 100, 100);
    background(0);
    strokeWeight(1);

    font = loadFont("Asuka.vlw");

    textFont(font, SCALE/2);
    textAlign(CENTER, CENTER);

    songName = songName.toString();
    println(songName);

    minim = new Minim(this);
    song = minim.loadFile(songName, 1024);
    fft = new FFT(song.bufferSize(), song.sampleRate());

    beat = new BeatDetect(song.bufferSize(), song.sampleRate());
    beat.detectMode(BeatDetect.FREQ_ENERGY);
    beat.setSensitivity(20);

    song.play();
    //song.skip(song.length()-20000);
}

void draw() {

    float   modX = map(dist(mouseX, mouseY, width/2, height/2), 0, width/2, 180, 360), 
    // modX = map(mouseX, 0, width,  0, 100),
    modY = map(mouseY, 0, height, 0, 100);

    translate(width/2, height/2);

    x1 = (int) random(width/2);
    y1 = (int) random(height/2);
    x2 = (int) random(width/2);
    y2 = (int) random(height/2);

    int hue = int(modX), 
    sat = int( map(x2, 0, width/2, 0, 25)  + map(y2, 0, height/2, 0, 25)      + modY ), 
    bri = 50 + int( map(x1+x2, 0, width, 0, 25) + map(y1 + y2, 0, height/2, 0, 25) - modY/2);

    c = color(hue, sat, bri, 25+randomGaussian()*50);

    //float decide = random(1) + modY/1000;

    if(song.isPlaying()) doStuff();
    
    if(song.position() <= 1000 * 20) {
        noStroke();
        fill(0, map(song.position(), 0, 1000 * 20, 100, 0) );

        pushMatrix();
        translate(-width/2, -height/2);
        rect(0, 0, width, height);
        popMatrix();

        noFill();
    }
    else if(song.position() >= song.length() - 1000 * 20) {
        noStroke();
        fill(0, map(song.position(), song.length()- 1000 * 20, song.length()-4000, 0, 100) );

        pushMatrix();
        translate(-width/2, -height/2);
        rect(0, 0, width, height);
        popMatrix();

        noFill();
    }
    
    if(song.position() >= song.length() - 4000) exit();
}

void mousePressed() {
    println(song.position());
    if (song.isPlaying()) {
        song.pause();
    } else {
        song.play();
    }
}

void keyPressed() {
    if (key == 's' || key == 'S') {
        saveFrame("Mandala####.png");
    }
    else if(key == 'f' || key == 'F') {
        song.skip(1000);
    }
}

void stop() {
    song.close();
}


void doStuff() {
    
    float condLevel = 0.275;
    boolean condition = (song.mix.level() < condLevel);

    if (condition) {
        strokeWeight( int(random( 2, 4 )) );
        stroke(c);
    } else {

        strokeWeight( 1 );
        noStroke();
        fill(0, 50 + randomGaussian()*20);

        pushMatrix();
        translate(-width/2, -height/2);
        rect(0, 0, width, height);
        popMatrix();

        noFill();

        float vol = map(song.mix.level(), 0, 1, 0, 100);
        stroke(360 - hue(c), 100 - saturation(c), random(70, 100), 100-vol*randomGaussian());
    }

    int j = (int) random(1, 4);
    iterations = (int) pow(6, j);
    //iterations = (int) 8 * j;

    for ( i=0; i < iterations; i++) {
        line(x1, y1, x2, y2);
        rotate(2f*PI/iterations);
    }

    noFill();

    poly = new Polygon(beat.isOnset() ? 6 : 12, map(dist(x1, y1, x2, y2), 0, width, SCALE, height - PADDING));
    poly.draw(0, 0);

    if ((song.mix.level() > condLevel * 2)) {
        textFont(font, 80-20*song.mix.level());
        fill(    
            hue(c),
            saturation(c),
            100,
            map(dist(mouseX, mouseY, W/2, H/2), 0, width, 0, map(song.position(), 0, song.length(), 0, 80)) );
        smooth();
        text("BON VOYAGE", 0, SCALE*3);
        noSmooth();
        noFill();
    }

    
}
