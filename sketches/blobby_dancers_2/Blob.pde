class Blob {
  float radius, minRadius;
  
  int nPoints = 200;
  float angle = TWO_PI / nPoints;
  float noiseFactor = 0.002f;
  float timeFactor = 0.01f;
  PVector[] vertices;
  
  public Blob(float radius) {
    this.radius = radius;
    this.minRadius = radius * 0.2;
    this.vertices = new PVector[nPoints];
    
    for (int i = 0; i < nPoints; i++) {
      vertices[i] = calculateVertex(i);
    }
  }
  
  PVector calculateVertex(int i) {
    return new PVector(
        cos(angle * i) * radius,
        sin(angle * i) * radius
       ).normalize();
  }
  
  public void mutate(float t) {
    float n;
    
    for (PVector v : vertices) {
      n = noise(v.x * noiseFactor + t * timeFactor, v.y * noiseFactor + t * timeFactor);
      v.setMag(map(n, 0, 1, minRadius, radius));
    }
  }
  
  public void paint(float x, float y) {
    pushMatrix();
    translate(x, y);
    beginShape();
    
    for (PVector v : vertices) {
      fill(palette.getColor(v.mag()));
      // fill(palette.getColor(v.mag() / radius));
      curveVertex(v.x, v.y);
    }

    endShape(CLOSE);
    popMatrix();
  }
  
  public void paint(PVector o) {
    paint(o.x, o.y);
  }
}
