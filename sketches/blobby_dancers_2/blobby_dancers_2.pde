Blob b;
BlobbyDancer[] dancers;

int radius = 50;

int N_DANCERS = 8;

void setup() {
  background(0);
  // size(800, 600, P2D);
  size(800, 600);
  noStroke();
  smooth(10);
  
  blendMode(DIFFERENCE);
  
  init();
  
  // colorMode(HSB, 360, 100, 100, 100);
  
  b = new Blob(radius);
  
  PVector center = new PVector(width / 2, height / 2);
  dancers = new BlobbyDancer[N_DANCERS];
  
  for (int i = 0; i < N_DANCERS; ++i) {
    dancers[i] = new BlobbyDancer(center, PVector.random2D(), random(radius), 2);
  }
}

void draw() {
  // background(0);
  palette.drawBackground(2);
  float a = radians(frameCount % 360);
  // translate(width >> 1, height >> 1);
  // b.paint(cos(a) * width / 3, sin(a) * height / 3);
  // b.paint(mouseX, mouseY);
  // b.mutate(frameCount);
  
  for (Dancer dancer : dancers) {
    dancer.update();
    dancer.draw(1, #00ff00);
  }
}
