boolean texturize = true;

void texturizeAll () {
  if (texturize) {
    noiseTexture(50, 50);
    noiseTexture(30, 30);
    // stainsTexture(10, 5);
    // scratchesOptimizedTexture(30, 100, 10); // FIREFLIES!
    scratchesOptimizedTexture(10, width, 30, .5);
    // scratchesTexture(10, 7, 15, 200, 10, 20);
    grainTexture(10, false);
  }
}

/**
 * Another way to adjust the character of the resulting sequence is
 * the scale of the input coordinates. As the function works within
 * an infinite space, the value of the coordinates doesn't matter as
 * such; only the distance between successive coordinates is
 * important (such as when using noise() within a loop). As a general
 * rule, the smaller the difference between coordinates, the smoother
 * the resulting noise sequence. Steps of 0.005-0.03 work best for
 * most applications, but this will differ depending on use.
 **/
 
 
void noiseTexture(float alpha, float smoothness) {
  PImage img = createImage(width, height, ARGB);
  img.loadPixels();
  float step = 1;
  float z = random(height);
  for (int x = 0; x < width; x += step) {
    for (int y = 0; y < height; y += step) {
      float noiseValue = noise(x / smoothness, y / smoothness, z / smoothness);
      img.set(x, y, color(0, noiseValue * alpha));
    }
  }
  img.updatePixels();
  image(img, 0, 0);
}

void grainTexture(float alpha, boolean blur) {
  PImage img = createImage(width, height, ARGB);
  img.loadPixels();
  float step = 1;
  float z = random(height);
  for (int x = 0; x < width; x += step) {
    for (int y = 0; y < height; y += step) {
      float noiseValue = noise(x, y, z);
      img.set(x, y, color(0, noiseValue * alpha));
    }
  }
  img.updatePixels();
  
  if (blur) img.filter(BLUR);
  
  image(img, 0, 0);
}

void scratchesOptimizedTexture(float alpha, int amount, int distance, float deviation) {
  PVector[] vertices   = new PVector[amount];
  PVector[] directions = new PVector[amount];
  
  for (int i = 0; i < vertices.length; i++) {
    vertices[i]   = new PVector(noise(frameCount, i) * width, noise(i, frameCount) * height);
    directions[i] = new PVector(randomGaussian(), randomGaussian());// ).normalize();
  }
  pushStyle();
  
  noFill();
  stroke(255, alpha);
  
  deviation = 1.0 / deviation;
   
  for (int times = 0; times < distance; times++) {
    stroke(palette.getColor((float)times / distance), alpha);
    strokeWeight(times / 10.0);
    
    for (int i = 0; i < vertices.length; i++) {
      vertices[i].add(new PVector(randomGaussian(), randomGaussian()).mult(deviation));
      vertices[i].add(directions[i]);
      point(vertices[i].x, vertices[i].y);
    }
  }
  
  popStyle();
}

void scratchesTexture(float alpha, float scale, float minSize, float maxSize, float minStep, float maxStep) {
  pushStyle();
  strokeWeight(.8);
  
  for (int x = 0; x < width; x += random(minStep, maxStep)) {
    for (int y = 0; y < height; y += random(minStep, maxStep)) {
      if (chance(85)) {
        float positionX = x;
        float positionY = y;
        float iterations = random(5);
        // float scale  = random(200, 300);
        float size   = random(minSize, maxSize);
        float offset = random(TWO_PI);
        beginShape(LINES);
       
       for (int i = 0; i < iterations; i++) {
          stroke(lerpColor(get(x, y), color(chance(50) ? 0 : 255), random(.3, .7)), alpha);
          float noiseValue = offset + noise(positionX / scale, positionY / scale) * TWO_PI;
          float noiseX = positionX + cos(noiseValue) * size;
          float noiseY = positionY + sin(noiseValue) * size;
          vertex(positionX, positionY);
          vertex(noiseX, noiseY);
          positionX = noiseX;
          positionY = noiseY;
        }
        endShape();
      }
    }
  }
  
  popStyle();
}

void stainsTexture(float alpha, float count) {
  float remaining = count; 
  while (remaining > 0) {
    float x = random(width);
    float y = random(height);
    
    if (noise(x / height, y / height) > 0.5) { 
      
      pushMatrix();
      pushStyle();
      translate(x, y);
      
      noStroke();
      
      float parts = random(1, 5);
      for (int j = 0; j < parts; j++) {
        float size = j == 0 ? random(10, 30) : random(3, 10);
        float ix = x + randomGaussian() * size;
        float iy = y + randomGaussian() * size;
        
        fill(random(100, 150), alpha + randomGaussian() * alpha / 10);
        beginShape();
        
        float step = TWO_PI / random(5, 8);
        float a = 0;
        curveVertex(ix + cos(a) * size, iy + sin(a) * size);
        for (; a < TWO_PI; a += step) {
          float rx = randomGaussian() * size / 5;
          float ry = randomGaussian() * size / 5;
          curveVertex(ix + rx + cos(a) * size, iy + ry + sin(a) * size);
        }
        curveVertex(ix + cos(a) * size, iy + sin(a) * size);
        endShape(CLOSE);
      }
      
      popStyle();
      popMatrix();
    }
    remaining--;
  }
}
