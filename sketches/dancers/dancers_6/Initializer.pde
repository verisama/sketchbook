import dawesometoolkit.*;
DawesomeToolkit dawesome;

Palette palette;

void init () {
  palette = new Palette(new color[] { #99d5c9, #6c969d, #645e9d, #392b58 }, PaletteLerpMode.LINEAR); // generated with https://coolors.co/app
  palette.setBackground(#2d0320);
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}
