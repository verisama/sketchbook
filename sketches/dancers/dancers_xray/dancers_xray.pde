int MODE = 1;
int N_DANCERS = 3; // Try 16

int ALPHA_MIN = 0, ALPHA_MAX = 64;
int SPEED_MIN = 0, SPEED_MAX = 1;

float BG_OPACITY = 1; // Try 8

AlphaDancer[] alphaDancers; 

void setup () {
  init();
  size(600, 600);
  
  palette.drawBackground(255);
  blendMode(ADD);
  
  prepare();
  
  /** STOPPED MODE */
  if (MODE == 0) {
    palette.draw();
  }
  /** REGULAR MODE */
  else if (MODE == 1) {
    
  }
  /** DEBUG MODE */
  else if (MODE == 9) {
    palette.draw();
  }
}


void draw () {
  /** STOPPED MODE */
  if (MODE == 0) {
    return;
  }
  /** REGULAR MODE */
  else if (MODE == 1) {
    
    palette.drawBackground(BG_OPACITY);
    updateAlphaDancers();
  }
  /** DEBUG MODE */
  else if (MODE == 2) {
    palette.draw();
  }
}


void updateAlphaDancers () {
  for (int i = 0; i < alphaDancers.length; i++) {
    alphaDancers[i].setAlpha(map(mouseY, 0, height, ALPHA_MIN, ALPHA_MAX));
    alphaDancers[i].setSpeed(map(mouseX, 0, width, SPEED_MIN, SPEED_MAX));
    alphaDancers[i].update();
    alphaDancers[i].draw(3, palette.getColor((float) i / alphaDancers.length * (0.5 + alphaDancers[i].direction.x + alphaDancers[i].direction.y)));
  }
}

void prepare() {
  PVector center = new PVector(width / 2, height / 2);
  
  alphaDancers = new AlphaDancer[N_DANCERS];
  for (int i = 0; i < alphaDancers.length; i++) {
    float angle = radians(360.0 / (float) alphaDancers.length * i);
    float size = random(10, 80);
    float speed = random(1, 5) / 10;
    PVector direction = new PVector(cos(angle), sin(angle));
    
    alphaDancers[i] = new AlphaDancer(center, direction, size, speed);
  }
}
