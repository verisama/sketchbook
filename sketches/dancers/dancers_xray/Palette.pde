enum PaletteLerpMode { LINEAR, LOG, EXP };

class Palette {
  color[] colors;
  color foreground;
  color background;
  PaletteLerpMode mode;
  
  /**
  * Constructors
  */
  
  public Palette (color[] colors) {
    this.colors = colors;
    this.mode = PaletteLerpMode.LINEAR;
  }
  
  public Palette (color[] colors, PaletteLerpMode mode) {
    this.colors = colors;
    this.mode = mode;
  }

  
  /**
  * Get Color Methods
  */
  
  color getColor () { return getColor(noise(frameCount, random(1))); }
  
  color getColor(float v) { return getColor(v, mode); }
  
  color getColor(float v, PaletteLerpMode mode) { return getColor(v, colors.length, mode); }
  
  color getColor(float v, float max, PaletteLerpMode mode) {
    v = constrainValue(v, max, mode);
    
    float c1 = floor(v * colors.length);
    float c2 = ceil(v * colors.length);
    
    if (c1 == c2) { c2++; }
    
    v = map(v, c1 / colors.length, c2 / colors.length, 0, 1);
    
    if (c2 == colors.length) c2 = 0;
    
    return lerpColor(colors[(int)c1], colors[(int)c2], v);
  }
  
  
  float constrainValue (float v, float max, PaletteLerpMode mode) {
    v = abs(v);
    
    if (v > 1) { v = (v % max) / max; }
    
    switch (mode) {
      case LINEAR: break;
      case EXP: v = map(exp(v), exp(0), exp(1), 0, 1); break;
      case LOG: v = v * log(v); break;
    }
    
    v = v % 1.0;
    
    return v;
  }
  
  color getColorDiscrete(float v) { return getColorDiscrete(v, mode); }
  
  color getColorDiscrete(float v, int size) { return getColorDiscrete(v, size, mode); }
  
  color getColorDiscrete(float v, PaletteLerpMode mode) { return getColorDiscrete(v, colors.length, mode); }
  
  color getColorDiscrete(float v, int size, PaletteLerpMode mode) {
    return getColor(floor(v * size) / (float)size, mode);
  }
  

  /**
  * Special Color Methods
  */
  
  void setForeground(color c) { this.foreground = c; }
  void setBackground(color c) { this.background = c; }
  color fg() { return foreground; }
  color bg() { return background; }
 
  
  /**
  * Debug Methods
  */
  
  void draw () {
    pushStyle();
    noFill();
    strokeWeight(1);
    for (int x = 0; x < width; x++) {
      float v = (float) x / width;
      color c = getColorDiscrete(v, 36);
      stroke(c);
      line(x, 0, x, height);
    }
    popStyle();
  }
  
  void drawBackground (float alpha) {
    pushMatrix();
    pushStyle();
    noStroke();
    fill(background, alpha);
    rect(0, 0, width, height);
    popStyle();
    popMatrix();
  }
}
