int SCALE = 10;

PVector eye, focus, step;
ArrayList<Body> bodies;

void prepare () {
  bodies = new ArrayList<Body>();
  
  eye   = new PVector(0, 3 * SCALE, SCALE);
  focus = new PVector(SCALE * 5, SCALE * 5, 0);
  step  = new PVector(SCALE * 0.75, SCALE * 0.8, SCALE);
}

void setup () {
  frameRate(4);
  
  init();
  
  size(800, 600, P3D);
  background(0);
  noStroke();
  fill(255);
  
  prepare();
}

void draw () {
  background(0);
  lights();
    
  focus.add(step.x, step.y, step.z);
  
  // camera(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)
  camera(eye.x + focus.x, eye.y + focus.y, eye.z + focus.z,
         focus.x, focus.y, focus.z,
         0.0, -1.0, 0.0);
  
  eye.z += step.z;
  
  addBody(focus, new PVector(random(SCALE * 2), random(SCALE * 2), random(SCALE * 2)));
  drawBodies();
}

void addBody (PVector origin, PVector scale) {
  Body b = new Body(origin.copy(), scale);
  bodies.add(b);
}

void drawBodies () {
  for (Body b : bodies) {
    b.draw(1);
  }
}
