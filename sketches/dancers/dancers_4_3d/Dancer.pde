class Dancer3D {
  Transform3D transform;
  PVector direction;
  
  float speed, rotationSpeed;
  
  int lifetime;
  
  boolean dead;

  public Dancer3D (PVector origin, PVector size, PVector direction, float speed) {
    this.transform = new Transform3D(origin, size);
    this.direction = direction;
    
    this.speed = speed;
    this.rotationSpeed = speed;
    
    this.dead = false;
    this.lifetime = 0;
  }
  
  public Dancer3D (PVector origin, PVector size) {
    this.transform = new Transform3D(origin, size);
    this.direction = new PVector(0, 0, 0);
    
    this.speed = 0;
    this.rotationSpeed = speed;
    
    this.dead = false;
    this.lifetime = 0;
  }
  
  
  void update () {
    if (dead) return;
    
    lifetime++;
  }
   
  void draw (int mode) {
    if (dead) return;

    transform.beginMatrix();
    
    switch (mode) {
      case 1: draw1(); break;
      case 2: draw2(); break;
      case 3: draw3(); break;
    }
    
    transform.endMatrix();
  }
  
  void draw1 () {
    box(transform.scale.x, transform.scale.y, transform.scale.z);
  }
  
  void draw2 () {
    box(transform.scale.x, transform.scale.y, transform.scale.z);
  }
  
  void draw3 () {
    sphere(transform.scale.x);
  }
}
