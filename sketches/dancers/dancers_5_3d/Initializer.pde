import dawesometoolkit.*;
DawesomeToolkit dawesome;

Palette palette;

void init () {
  palette = new Palette("1cb56b-46cc35-86e7b8-93ff96-b2ffa8-d0ffb7-f2f5de", PaletteLerpMode.LINEAR); // generated with https://coolors.co/app
  palette.setBackground(#E63946);
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}
