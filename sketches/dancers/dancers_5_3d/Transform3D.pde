class Transform3D {
  PVector position;
  PVector scale;
  
  float x, y, z;

  /**
  * Constructors
  */
  
  public Transform3D () {
    this.position = new PVector(0, 0, 0);
    this.scale    = new PVector(1, 1, 1);
    
    updateXYZ();
  }
  
  public Transform3D (PVector position) {
    this.position = position.copy();
    this.scale    = new PVector(1, 1, 1);
    
    updateXYZ();
  }
  
  public Transform3D (PVector position, PVector scale) {
    this.position = position.copy();
    this.scale    = scale.copy();
    
    updateXYZ();
  }
  
  
  /**
  * Public methods
  */
 
  void scale (PVector factor) {
    scale = scale.cross(factor);
  }
  
  void move (PVector direction, float speed) {
    position.add(direction.copy().mult(speed));
    
    updateXYZ();
  }
  
  void move (PVector direction) {
    move(direction, 1);
  }
  
  void beginMatrix() {
    pushMatrix();
    translate(position.x, position.y, position.z);
  }
  
  void endMatrix() {
    popMatrix();
  }
  
  void updateXYZ() {
    x = position.x;
    y = position.y;
    z = position.z;
  }
  
  boolean inBounds (float x0, float y0, float x1, float y1) {
    float minX = min(x0, x1), maxX = max(x0, x1);
    float minY = min(y0, y1), maxY = max(y0, y1);
    
    return position.x > minX && position.x < maxX
        && position.y > minY && position.y < maxY;
  }
  
  boolean inBounds (float x0, float y0, float z0, float x1, float y1, float z1) {
    float minX = min(x0, x1), maxX = max(x0, x1);
    float minY = min(y0, y1), maxY = max(y0, y1);
    float minZ = min(z0, z1), maxZ = max(z0, z1);
    
    return position.x > minX && position.x < maxX
        && position.y > minY && position.y < maxY
        && position.z > minZ && position.z < maxZ;
  }
}
