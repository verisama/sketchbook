ArrayList<Body> bodies;
float scale = 1;

void prepare () {
  bodies = new ArrayList<Body>();
  for (int i = 0; i < 20; i++) {
    addBody(new PVector(0, 0, 0), new PVector(random(100), random(100), random(100)));
  }
}

void setup () {
  init();
  
  size(800, 600, P3D);
  background(0);
  noStroke();
  fill(255);
  
  prepare();
}

void draw () {
  // background(0);
  lights();
  translate(width / 2, height / 2);
  rotate(radians(frameCount % 360));
  
  scale(scale += round(random(-1, 1)) * noise(frameCount % 360) / 100);
  
  
  drawBodies();  
}

void addBody (PVector origin, PVector scale) {
  Body b = new Body(origin.copy(), scale);
  bodies.add(b);
}

void drawBodies () {
  for (Body b : bodies) {
    b.transform.scale.add(new PVector(randomGaussian(), randomGaussian(), randomGaussian()));
    b.draw(1);
  }
}
