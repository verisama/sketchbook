class Body extends Dancer3D {
  color c;
  
  public Body (PVector origin, PVector size) {
    super(origin, size);
    c = palette.getColor();
  }
  
  public Body (PVector origin, PVector size, PVector direction, float speed) {
    super(origin, direction, size, speed);
    c = palette.getColor();
  }
  
  void update () {
    super.update();
  }
  
  void draw1 () {
    fill(c);
    super.draw1();
  }
}
