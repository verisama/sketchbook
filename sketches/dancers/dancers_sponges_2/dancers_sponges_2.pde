import milchreis.imageprocessing.*;

int MODE = 1;
int N_DANCERS = 5;
int ALPHA_DRAW = 2;

int ALPHA_MIN = 0, ALPHA_MAX = 255;
int SPEED_MIN = 0, SPEED_MAX = 3;

float BG_OPACITY = 0;


AlphaDancer[] alphaDancers; 

float COLOR_FACTOR_1 = 10;

void setup () {
  init();
  size(600, 600);

  palette.drawBackground(255);
  // blendMode(SCREEN);

  prepare();

  /** STOPPED MODE */
  if (MODE == 0) {
    palette.draw();
  }
  /** REGULAR MODE */
  else if (MODE == 1) {
  }
  /** DEBUG MODE */
  else if (MODE == 9) {
    palette.draw();
  }
}


void draw () {
  /** STOPPED MODE */
  if (MODE == 0) {
    return;
  }
  /** REGULAR MODE */
  else if (MODE == 1) {
    palette.drawBackground(BG_OPACITY);
    updateAlphaDancers();

    // replicateFractal();
  }

  /** DEBUG MODE */
  else if (MODE == 2) {
    palette.draw();
  }
}


void replicate () {
  PImage current = get();
  current.resize(width / 2, height / 2);
  image(current, 0, 0);
}

void replicateFractal () {
  PImage current = get();
  current.resize(width / 2, height / 2);

  /* Fractal */
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      image(current, width / 2 * i, height / 2 * j);
    }
  }
}

void updateAlphaDancers () {
  for (int i = 0; i < alphaDancers.length; i++) {
    alphaDancers[i].setAlpha(map(mouseY, 0, height, ALPHA_MIN, ALPHA_MAX));
    alphaDancers[i].setSpeed(map(mouseX, 0, width, SPEED_MIN, SPEED_MAX));
    alphaDancers[i].update();
    alphaDancers[i].draw(ALPHA_DRAW, palette.getColor((float) i / alphaDancers.length * (alphaDancers[i].lifetime / COLOR_FACTOR_1)));
  }
}

void prepare() {
  PVector center = new PVector(width / 2, height / 2);
 
  alphaDancers = new AlphaDancer[N_DANCERS];
  for (int i = 0; i < alphaDancers.length; i++) {
    float angle = radians(360.0 / (float) alphaDancers.length * i);
    float size = random(10, 80);
    float speed = random(1, 5) / 10;
    PVector direction = new PVector(cos(angle), sin(angle));

    alphaDancers[i] = new AlphaDancer(center, direction, size, speed);
  }
}
