class AlphaDancer extends Dancer {
  PVector[] vertices;

  float strokeWeight = 4;
  float alpha = 10;

  float[] angles;

  public AlphaDancer (PVector origin, PVector direction, float size, float speed) {
    super(origin, direction, size, speed);

    angles = new float[5];
    float total = 360;

    for (int i = 1; i < angles.length; i++) { 
      float r = random(total); 
      angles[i] = total - r; 
      total -= r;
    } 

    angles[0] = max(total, 0);

    transform.rotation = radians(random(360));
    setVertices();
  }


  void setVertices () {
    vertices = new PVector[angles.length];

    for (int i = 0; i < angles.length; i++) {
      vertices[i] = new PVector(cos(angles[i] + transform.rotation) * transform.scale.x, 
        sin(angles[i] + transform.rotation) * transform.scale.y);
    }
  }


  void update () {
    super.update();
    updateDirection(true);
    updatePosition();
    updateRotation();
    updateScale();
  }

  void updateDirection (boolean rand) {
    if (rand) {
      direction.x += noise(direction.x, direction.y) / 100;
      direction.y += noise(direction.x, direction.y) / 100;
    }
    else {

    }

    direction.normalize();
  }

  void updatePosition () {
    transform.move(direction, speed);

    for (PVector v : vertices) {
      v.add(direction.copy().mult(speed));
    }

    if (!transform.inBounds(0, 0, width, height)) {
      invertDirection();
    }
  }

  void updateRotation () {
    // transform.turnDegrees(3 * noise(transform.position.x, transform.position.y));
  }

  void updateScale () {
    // transform.scale.add(new PVector(direction.heading(), direction.heading()).mult(speed / 10.0));

    // setVertices();
  }

  void setSpeed (float v) { 
    speed = v;
  }

  void setAlpha (float v) { 
    alpha = v;
  }

  void invertDirection () {
    direction.x = -direction.x;
    direction.y = -direction.y;
  }

  void draw1 (color c) {
    pushStyle();
    noFill();
    strokeWeight(strokeWeight);
    stroke(c, alpha);

    beginShape();
    for (PVector v : vertices) { 
      vertex(v.x, v.y);
    }
    endShape(CLOSE);

    popStyle();
  }

  void draw2 (color c) {
    pushStyle();

    if (randomGaussian() < 2 ) { 
      noStroke();
    }
    else {
      strokeWeight(speed * 5);  
      stroke(palette.lightest());
    }

    fill(c, alpha);

    rect(random(vertices[0].x, vertices[1].x), random(vertices[0].y, vertices[1].y), 
      random(vertices[1].x, vertices[2].x), random(vertices[1].y, vertices[2].y));


    popStyle();
  }

  void draw3 (color c) {
    pushStyle();
    noStroke();
    fill(c, alpha);

    float sizes[] = new float[]{ 5 + randomGaussian() * 3, 8 + randomGaussian() * 5, 13 + randomGaussian() * 8 };

    for (int i = 0; i < sizes.length; i++) {
      float size = sizes[i] / 108 * dist(vertices[0].x, vertices[0].y, vertices[1].x, vertices[1].y);
      ellipse(vertices[i].x, vertices[i].y, size, size);
    }

    noFill();
    strokeWeight(strokeWeight);
    stroke(c, alpha);

    line(vertices[0].x, vertices[0].y, (vertices[1].x + vertices[2].x) / 2, (vertices[1].y + vertices[2].y) / 2);


    popStyle();
  }
}
