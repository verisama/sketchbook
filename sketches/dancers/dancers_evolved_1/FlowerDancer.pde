class FlowerDancer extends Dancer {
  Flower flower;

  float strokeWeight = 4;
  float alpha = 10;

  public FlowerDancer (PVector origin, PVector direction, float size, float speed, int nPetals) {
    super(origin, direction, size, speed);
    
    flower = new Flower(nPetals, new PVector(0, 0), size, speed * 360, size * speed * millis());
    
    transform.rotation = radians(random(360));
  }
  
  void update () {
    super.update();
    updateDirection();
    updatePosition();
    updateRotation();
    // updateScale();
  }
  
  void updateDirection () {
    // direction.x += (random(1) > .5 ? -1 : 1) * noise(direction.x);
    // direction.y += (random(1) > .5 ? -1 : 1) * noise(direction.y);
    
    direction.x += randomGaussian() / 100;
    direction.y += direction.x * randomGaussian();
  
    direction.normalize();
  }
  
  void updatePosition () {
    transform.move(direction, speed);
    
    if (!transform.inBounds(0, 0, width, height)) {
      invertDirection();
      transform.position = new PVector(random(width), random(height));
    }
  }
  
  void updateRotation () {
    transform.turnDegrees(3 * noise(transform.position.x, transform.position.y));
  }
  
  void updateScale () {
    transform.scale.add(new PVector(direction.heading(), direction.heading()).mult(speed / 10.0));
  }
  
  void setSpeed (float v) { speed = v; }
  
  void setAlpha (float v) { alpha = v; }
  
  void invertDirection () {
    direction.x = -direction.x;
    direction.y = -direction.y;
  }
  
  void draw1 (color c) {
    pushStyle();
    noFill();
    strokeWeight(strokeWeight);
    
    float t = (frameCount % flower.cycleLength) / flower.cycleLength;
    t = abs(0.5 - t) * 2;
    
    stroke(palette.getColor(t), alpha);
    
    flower.petal(t);
    
    popStyle();
  }
  
  void draw2 (color c) {
    pushStyle();
    noStroke();
    fill(c, alpha);
    
    
    popStyle();
  }
  
  void draw3 (color c) {
    pushStyle();
    noStroke();
    fill(c, alpha);
    
    
    popStyle();
  }
}
