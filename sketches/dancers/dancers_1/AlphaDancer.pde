class AlphaDancer extends Dancer {
  PVector[] vertices;

  float strokeWeight = 4;
  float alpha = 10;

  public AlphaDancer (PVector origin, PVector direction, float size, float speed) {
    super(origin, direction, size, speed);
    
    transform.rotation = radians(random(360));
    setVertices(radians(transform.rotation + 0), radians(transform.rotation + 120), radians(transform.rotation + 240));
  }
  
  
  void setVertices (float angle1, float angle2, float angle3) {
    vertices = new PVector[] {
      new PVector(cos(angle1) * transform.scale.x, sin(angle1) * transform.scale.y),
      new PVector(cos(angle2) * transform.scale.x, sin(angle2) * transform.scale.y),
      new PVector(cos(angle3) * transform.scale.x, sin(angle3) * transform.scale.y)
    };
  }
  
  
  void update () {
    super.update();
    updateDirection(true);
    updatePosition();
    updateRotation();
    updateScale();
  }
  
  void updateDirection (boolean rand) {
    if (rand) {
      direction.x += randomGaussian() / 100;
      direction.y += direction.x * randomGaussian();
    }
    else {
      
    }
    
    direction.normalize();
  }
  
  void updatePosition () {
    transform.move(direction, speed);
    
    for (PVector v : vertices) {
      v.add(direction.copy().mult(speed));
    }
    
    if (!transform.inBounds(0, 0, width, height)) {
      invertDirection();
    }
  }
  
  void updateRotation () {
    transform.turnDegrees(3 * noise(transform.position.x, transform.position.y));
  }
  
  void updateScale () {
    transform.scale.add(new PVector(direction.heading(), direction.heading()).mult(speed / 10.0));
    
    setVertices(radians(transform.rotation + 0), radians(transform.rotation + 120), radians(transform.rotation + 240));
  }
  
  void setSpeed (float v) { speed = v; }
  
  void setAlpha (float v) {
    alpha = v;
  }
  
  void invertDirection () {
    direction.x = -direction.x;
    direction.y = -direction.y;
  }
  
  void draw1 (color c) {
    pushStyle();
    noFill();
    strokeWeight(strokeWeight);
    stroke(c, alpha);
    
    triangle(vertices[0].x, vertices[0].y,
             vertices[1].x, vertices[1].y,
             vertices[2].x, vertices[2].y);
    
    
    popStyle();
  }
  
  void draw2 (color c) {
    pushStyle();
    noStroke();
    fill(c, alpha);
    
    triangle(random(vertices[0].x, vertices[1].x), random(vertices[0].y, vertices[1].y),
             random(vertices[1].x, vertices[2].x), random(vertices[1].y, vertices[2].y),
             random(vertices[2].x, vertices[0].x), random(vertices[2].y, vertices[0].y));
    
    
    popStyle();
  }
  
  void draw3 (color c) {
  
  }
}
