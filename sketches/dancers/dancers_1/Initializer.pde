import dawesometoolkit.*;
DawesomeToolkit dawesome;

Palette palette;

void init () {
  palette = new Palette(new color[] { #721121, #a5402d, #f15156, #ffc07f, #ffcf99, #F18805, #F0A202 }, PaletteLerpMode.LINEAR); // generated with https://coolors.co/app
  palette.setBackground(#9FD356);
  
  dawesome = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png"); // Creates a unique time based filename and has a 500ms debounce built-in.
}
