class DancerOrchestrator extends Orchestrator {
  FlowerDancer[] flowerDancers;
  
  int ALPHA_MIN = 20, ALPHA_MAX = 64;
  int SPEED_MIN = 1, SPEED_MAX = 3;
  int N_FLOWER_DANCERS = 5;
  
  public DancerOrchestrator (PApplet context, Media media) {
    super(context, media);
    
    this.dynamicLevel = true;
    this.flowerDancers = new FlowerDancer[N_FLOWER_DANCERS];
  }
  
  public void setup () {
    colorMode(HSB, 360, 100, 100, 100);
    
    background(30, 100, 100, 100); // DEBUG
    
    for (int i = 0; i < flowerDancers.length; i++) {
      
      float angle = radians(360.0 / (float) flowerDancers.length * i);
      float size = random(height / 10, height / 2);
      float speed = random(5);
      PVector direction = new PVector(cos(angle), sin(angle));
      
      PVector origin = new PVector(random(size * 2, width - size * 2), random(size * 2, height - size * 2));
   
      if (i > 0) {
        float x1 = flowerDancers[i-1].transform.position.x;
        float y1 = flowerDancers[i-1].transform.position.y;
          
        while ((origin.x != x1 || origin.y != y1) && dist(origin.x, origin.y, x1, y1) < 0) {
          origin = new PVector(random(size * 2, width - size * 2), random(size * 2, height - size * 2));
        }
      }
      
      flowerDancers[i] = new FlowerDancer(origin, direction, size, speed, i * 3);
    }
  }
  
  public void onBeat () {
    super.onBeat();
    
    background(0);
  }
  
  public void onLevelPeak () {
    super.onLevelPeak();
    
    background(255);
    for (int i = 0; i < 200; i++) updateDancers();
  }
  
  public void onLevelMid () {
    super.onLevelMid();
    
    setDancersAlpha(map(sinceLastPeak(), 0, 10000, ALPHA_MAX, ALPHA_MIN));
    for (int i = 0; i < 200; i++) updateDancers();
  }
  
  public void onLevelLow () {
    super.onLevelLow();
    
  }
  
  
  public void onKick () {
    super.onKick();
    
    background(#0000ff);
    ellipse(width/2, height/2, 40, 40);
  }
  public void onSnare () {
    super.onSnare();
    
    background(#00ff00);
    ellipse(width/2, height/2, 40, 40);
  }
  public void onHat () {
    super.onHat();
    
    background(#ff0000);
    ellipse(width/2, height/2, 40, 40);
  }
  
  
  public void always () {
    super.always();
    
    updateDancers();
    
    setDancersSpeed(map(sinceLastMid(), 0, 5000, SPEED_MAX, SPEED_MIN));
  }
  
  private void setDancersAlpha(float alpha) {
    for (int i = 0; i < flowerDancers.length; i++) {
      flowerDancers[i].setAlpha(constrain(alpha, ALPHA_MIN, ALPHA_MAX));
    }
  }
  
  private void setDancersSpeed(float speed) {
    for (int i = 0; i < flowerDancers.length; i++) {
      flowerDancers[i].setAlpha(constrain(speed, SPEED_MIN, SPEED_MAX));
    }
  }
  
  private void updateDancers () {
    for (int i = 0; i < flowerDancers.length; i++) {
      // flowerDancers[i].setAlpha(map(sinceLastMid(), 0, 5000, ALPHA_MAX, ALPHA_MIN));
      // flowerDancers[i].setSpeed(map(sinceLastPeak(), 0, 10000, SPEED_MAX, SPEED_MIN));
      flowerDancers[i].update();
      flowerDancers[i].draw(1, palette.getColor((float) i / flowerDancers.length));
    }
  }

}
