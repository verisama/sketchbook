/* OpenProcessing Tweak of *@*http://www.openprocessing.org/sketch/9584*@* */
/* !do not delete the line above, required for linking your tweak if you upload again */

import ddf.minim.*;
import ddf.minim.analysis.*;

public static final float SCALE = 80;
public static final int W = (int)SCALE * 16;
public static final int H = (int)SCALE * 9;

public static final int PADDING = W/20; //(px)

//FREQUENCY THRESHOLDS
/*
public static final int L_L_THRESHOLD = 10;
 public static final int L_U_THRESHOLD = 200;
 public static final int M_L_THRESHOLD = L_U_THRESHOLD;
 public static final int M_U_THRESHOLD = 4000;
 public static final int H_L_THRESHOLD = M_U_THRESHOLD;
 public static final int H_U_THRESHOLD = 15000;
 */

DancerOrchestrator orchestrator;
Media media;
AudioPlayer song;

void settings () {
  size(W, H);
  smooth();
}

void setup() {
  init();

  media = new Media(this, "mp3");
  song = media.loadAudio(media.getFile(1));
  song.skip(40 * 1000);
  orchestrator = new DancerOrchestrator(this, media);

  background(0);

  orchestrator.setup();
  orchestrator.play();
  //song.skip(song.length()-20000);
}

void draw() {
  orchestrator.orchestrate();

  if (media.position() >= media.length() - 4000) exit();
}


void stop() {
  media.stop();
}
