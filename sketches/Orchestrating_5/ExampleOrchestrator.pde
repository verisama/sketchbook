class ExampleOrchestrator extends Orchestrator {

  public ExampleOrchestrator (PApplet context, Media media) {
    super(context, media);

    this.dynamicLevel = true;
  }

  public void setup () {
    colorMode(HSB, 360, 100, 100, 100);
  }

  public void onBeat () {
    super.onBeat();

    fill(0);

    circle(width/2, height/2, 50);
  }

  public void onLevelPeak () {
    super.onLevelPeak();

    fill(360, 80, 80);
    circle(width/2, height/5, 50);
  }

  public void onLevelMid () {
    super.onLevelMid();

    fill(180, 80, 80);
    circle(width/2, height/5 * 2, 50);
  }

  public void onLevelLow () {
    super.onLevelLow();

    fill(90, 80, 80);
    circle(width/2, height/5 * 4, 50);
  }

  public void onBand (int band, float frequency, float power) {
    float intensity = power * scaleFactor * pow(log(band) * 3, 2);

    if (intensity < intensityThreshold) return;

    if (frequency > L_L_THRESHOLD && frequency <= L_U_THRESHOLD) {
      onLowFrequencies(band, frequency, intensity);

    } else if (frequency > M_L_THRESHOLD && frequency <= M_U_THRESHOLD) {
      onMidFrequencies(band, frequency, intensity);
    } else if (frequency > H_L_THRESHOLD && frequency <= H_U_THRESHOLD) {
      onHighFrequencies(band, frequency, intensity);
    }

    if (band == media.frequencyToBand(200)) {
      onKick(intensity);
    }
/*
    float hue = map(band, 0, media.FFT_BANDS, 0, 360);
    fill(hue, 80, 80);
    circle(map(band, 0, media.FFT_BANDS, 0, width), height/2, intensity);
    
    */
  }
  
  public void onKick(float intensity) {
    fill(0, 100, 100);
    circle(width / 2, height / 2, intensity * 20);
  }

  public void onLowFrequencies(int band, float frequency, float intensity) {
  }

  public void onMidFrequencies(int band, float frequency, float intensity) {
  }

  public void onHighFrequencies(int band, float frequency, float intensity) {
  }


  public void always () {
    super.always();

    background(0);
  }
}
