
//FREQUENCY THRESHOLDS
public static final int L_L_THRESHOLD = 10;
 public static final int L_U_THRESHOLD = 400;
 public static final int M_L_THRESHOLD = L_U_THRESHOLD;
 public static final int M_U_THRESHOLD = 4000;
 public static final int H_L_THRESHOLD = M_U_THRESHOLD;
 public static final int H_U_THRESHOLD = 15000;

class Orchestrator {
  PApplet context;
  Media media;

  float peakLevel = 0.65;
  float midLevel = 0.275;
  float lowLevel = 0.1;

  float lastBeat = 0;
  float lastPeak = 0;
  float lastMid = 0;
  float lastLow = 0;

  boolean dynamicLevel = true;
  String dynamicLevelMode = "normal";
  
  float smoothingFactor = 0.1;
  int scaleFactor = 10;
  float[] bands;
  float[] bandsPrevious;
  
  int intensityThreshold = 10;
  
  public Orchestrator (PApplet context, Media media) {
    this.context = context;
    this.media = media;

    if (dynamicLevel) {
      peakLevel = 0;
      midLevel = 0;
      lowLevel = 0;
    }
    
    bands = new float[media.FFT_BANDS];
  }

  public void setup () {
  }

  public void play () {
    media.play();
  }

  public void orchestrate () {
    if (!media.isPlaying()) return;

    always();

    if (media.isBeating()) onBeat();

    // Amplitude
    if (dynamicLevel) {
      if (dynamicLevelMode == "average") { peakLevel = (media.level() + peakLevel) / 2; }
      else { peakLevel = max(media.level(), peakLevel); }
      
      midLevel = peakLevel / 2;
      lowLevel = midLevel / 2;
    }

    if (media.level() >= peakLevel) onLevelPeak();
    else if (media.level() >= midLevel) onLevelMid();
    else if (media.level() >= lowLevel) onLevelLow();
    
    // FFT frequency analysis
    media.fft.analyze();
    for (int i = 0; i < media.FFT_BANDS; i++) {
      // Smooth the FFT spectrum data by smoothing factor
      bands[i] += (media.fft.spectrum[i] - bands[i]) * smoothingFactor;
  
      onBand(i, media.bandToFrequency(i), bands[i]);
    }
  }

  public void onBand (int band, float frequency, float power) {
    println("! band " + band + " frequency: " + frequency + " power: " + power);
  }
  

  public void onBeat () {
    lastBeat = media.position();
    
    println("! beat");
  }

  public void onLevelPeak () {
    lastPeak = media.position();
    
    // println("! peak");
  }

  public void onLevelMid () {
    lastMid = media.position();
    
    // println("! mid");
  }

  public void onLevelLow () {
    lastLow = media.position();
    
    // println("! low");
  }
  



  public void always () {
  }
  

  public float sinceLastBeat() {
    return media.position() - lastBeat;
  }

  public float sinceLastPeak() {
    return media.position() - lastPeak;
  }

  public float sinceLastMid() {
    return media.position() - lastMid;
  }

  public float sinceLastLow() {
    return media.position() - lastLow;
  }

  
  
}
